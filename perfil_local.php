
<html>
    <head>
        <meta charset="UTF-8">
        <link href="estilos/perfil_estilos.css" rel="stylesheet" type="text/css"/>
        <link href="estilos/estilosPropiosLocal.css" rel="stylesheet" type="text/css"/>
        <script src="script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="script/perfiles.js" type="text/javascript" defer></script>
        <script src="script/perfil_local_acciones.js" type="text/javascript"></script>
        <title>Perfil</title>
    </head>
    <body>
        <?php
        /* Iniciamos sesión, accedemos al fichero de bbdd, comprobamos si se ha logueado el usuario
          y guardamos los datos en caso afirmativo. */
        session_start();
        if (isset($_SESSION["username"])) {
            require 'bbdd/bbdd.php';
            $username = $_SESSION["username"];
            $idUsuario = recoger_ID_Usuario($username);
            $datosUsus = datosUsuario($idUsuario);
            $datosLocal = datosLocal($idUsuario);
            $ciudad = tuCiudad($idUsuario);
            $fotoActual = recogerFoto($idUsuario);
            ?>

            <!--Parte superior de la página-->
            <header>               
                <!--Botón para abrir y cerrar el menu-->
                <div id="barraSuperiorBloque1">
                    <div id="capsulaBoton">
                        <div id="botonMenu"> 
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                        </div>
                    </div>
                </div>

                <!--Título y logo-->
                <div id="barraSuperiorBloque2">
                    <img src="sources/Logo Valinor 2.png" alt="logoValinor"/> 
                    <div id="titulo"> VALINOR </div>
                </div>

                <!--Botón para visualizar datos, apartado de foto con modificaciones incluidas al apretar sobre la foto-->
                <div id="barraSuperiorBloque3">
                    <div id="misDatos"> Mis datos </div>
                    <div id="barraSeparadora"></div>                  
                    <div id="fotoPerfil">               
                        <img src="sources/<?php echo $fotoActual ?>" alt="foto_de_perfil"/>
                    </div>
                </div>   

                <!--Menu-->
                <nav id="menu">
                    <ul>
                        <li> <a class = "apartado" id="alta" href="#"> ALTA CONCIERTO </a> </li>
                        <li> <a class = "apartado" id="baja" href="#"> BAJA CONCIERTO </a> </li>
                        <li> <a class = "apartado" id="listado" href="#"> CONCIERTOS PROPUESTOS </a> </li>
                    </ul>
                </nav>
            </header>

            <!--Tabla con los datos del usu-->
            <div id="main"> <br>          
                <div id="datos">
                    <table border="1px black solid">
                        <tr> <td> <h1>Datos del perfil</h1> <!--<div id="cerrarMisDatos"> x </div> --> </td> </tr>
                        <tr> <td> <b> Nombre: </b> <?php echo $datosUsus["nombre"]; ?> </td> </tr>
                        <tr> <td> <b> Ciudad: </b> <?php echo $ciudad; ?> </td> </tr>
                        <tr> <td> <b> Ubicación: </b> <?php echo $datosLocal["ubicacion"]; ?> </td> </tr>
                        <tr> <td> <b> Aforo: </b> <?php echo $datosLocal["aforo"]; ?> </td> </tr>
                        <tr> <td> <b> E-Mail: </b> <?php echo $datosUsus["email"]; ?> </td> </tr>
                        <tr> <td> <b> Teléfono: </b> <?php echo $datosUsus["telefono"]; ?> </td> </tr>
                    </table>
                </div>
            </div><br>

            <!--Menu de modficiaciones al apretar la foto de perfil--> 
            <div id="usuario">
                <div id="triangulo"></div>
                <div id="opciones">
                    <ul>
                        <li><a href="modificarFoto.php">Cambiar foto de perfil</a></li>
                        <li><a href="modificarDatos.php">Modificar datos</a></li>
                        <li><a href="modificarPasswrd.php">Cambiar contraseña</a></li>
                        <li><a href="cerrar_session.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>

            <div id="alta-concierto">
                <h1>Crear Concierto</h1><br>
                <form method="POST">
                    <label>Nombre del concierto: </label>
                    <input type="text" name="nombreConcierto" minlength="1" maxlength="20" required><br><br>
                    <label>Fecha: </label>
                    <input type="date" name="fecha" required><br><br>
                    <label>Hora: </label>
                    <input type="time" name="hora" required><br><br>
                    <label>Género: </label>
                    <select name="genero" required>
                        <?php
                        $resultado = selectAllGeneros();
                        while ($fila = mysqli_fetch_assoc($resultado)) {
                            echo "<option value='" . $fila["idgenero"] . "'>" . $fila["nombre"] . "</option>";
                        }
                        ?>
                    </select><br><br>
                    <label>Precio entrada: </label>
                    <input type="number" name="entrada" min="0" required><br><br>
                    <label>Presupuesto: </label>
                    <input type="number" name="presupuesto" min="0" required><br><br>

                    <input type="submit" value="CREAR" name="crear">
                </form> 
                <?php
                if (isset($_POST["crear"])) {
                    $idlocal = $idUsuario;
                    $idgenero = $_POST["genero"];
                    $nombreConcierto = $_POST["nombreConcierto"];
                    $fecha = $_POST["fecha"];
                    $hora = $_POST["hora"];
                    $presupuesto = $_POST["presupuesto"];
                    $entrada = $_POST["entrada"];

                    $resultado = crearConcierto($idlocal, $idgenero, $nombreConcierto, $fecha, $hora, $presupuesto, $entrada);

                    if ($resultado === true) {
                        echo "Concierto creado";
                    } else {
                        echo $resultado;
                    }
                }
                ?>
            </div>

            <div id="baja-concierto">
                <h1>Cancelar Concierto</h1> 
                <?php
                if (conciertosPropuestos()) {
                    $conciertos = listadoConciertosPropuestos();
                    ?>
                    <table border="1px black solid">
                        <tr> <th> Nombre </th> <th> Género musical </th> <th> Fecha </th> <th> Presupuesto </th> </tr>
                        <?php
                        while ($fila = mysqli_fetch_assoc($conciertos)) {
                            echo '<tr>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . devolverGenero($fila["idgenero"]) . '</td>';
                            echo '<td>' . $fila["fecha"] . '</td>';
                            echo '<td>' . $fila["presupuesto"] . '€' . '</td>';
                            ?>
                            <td>
                                <form method="POST">
                                    <input type = "hidden" name = "idconcierto" value = "<?php echo $fila["idconcierto"] ?> ">
                                    <input type = "submit" id="cancelar" name = "cancelar" value = "Cancelar">
                                </form>
                            </td>
                            <?php
                        }
                        if (isset($_POST["cancelar"])) {
                            $idconcierto = $_POST["idconcierto"];
                            $cancelar = cancelarConcierto($idconcierto);

                            if ($cancelar == true) {
                                echo "Concierto cancelado";
                            } else {
                                echo $cancelar;
                            }
                        }
                    } else {
                        echo "No hay ningún concierto propuesto";
                    }
                    ?>
                </table>
            </div>

            <div id="listado-conciertos-propuestos">
                <h1>Conciertos propuestos</h1>
                <?php
                if (conciertosPropuestos()) {
                    $conciertos = listadoConciertosPropuestos();
                    ?>
                    <table border="1px black solid">
                        <tr> <th> Nombre </th> <th> Género musical </th> <th> Fecha </th> <th> Hora </th> <th> Entrada </th> <th> Presupuesto </th> <th> Solicitudes </th> </tr>
                        <?php
                        while ($fila = mysqli_fetch_assoc($conciertos)) {
                            echo '<tr>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . devolverGenero($fila["idgenero"]) . '</td>';
                            echo '<td>' . $fila["fecha"] . '</td>';
                            echo '<td>' . $fila["hora"] . '</td>';
                            echo '<td>' . $fila["entrada"] . '€' . '</td>';
                            echo '<td>' . $fila["presupuesto"] . '€' . '</td>';
                            ?>
                            <td>
                                <form method="POST">
                                    <input type = "hidden" name = "idconcierto2" value = "<?php echo $fila["idconcierto"] ?>">
                                    <input type="submit" name="verPropuestas" style="color: blue" value="<?php echo propuestasTotales($fila["idconcierto"]) ?> PROPUESTAS">
                                </form>
                            </td>
                            <?php
                            echo '</tr>';
                        }
                    } else {
                        echo "No hay ningún concierto propuesto";
                    }
                    ?>
                </table>
            </div>
            <?php
            if (isset($_POST["verPropuestas"])) {
                $idconcierto2 = $_POST["idconcierto2"];
                $datosMusicoSolicitante = propuestasConcierto($idconcierto2);
                ?>
                <div id="listadoPropuestas">
                    <h1>Propuestas de Músicos</h1>
                    <table border="1px black solid">
                        <tr> <th> Artista </th> <th> Contacto </th> <th> Propuesta </th> </tr>
                        <?php
                        while ($fila = mysqli_fetch_assoc($datosMusicoSolicitante)) {
                            echo '<tr>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . $fila["telefono"] . '</td>';
                            ?>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="idmusico" value="<?php echo $fila["idusuario"] ?>">
                                    <input type="hidden" name="idconcierto3" value="<?php echo $idconcierto2 ?>">
                                    <input type="submit" id="aceptar" name="aceptarPropuesta" style="color: blue" value="Aceptar">
                                </form>
                            </td>
                            <?php
                            echo '</tr>';
                        }
                        ?>
                    </table>
                </div>
                <?php
            }
            if (isset($_POST["aceptarPropuesta"])) {
                $idmusico = $_POST["idmusico"];
                $idconcierto3 = $_POST["idconcierto3"];
                aceptarMusico($idconcierto3, $idmusico);
            }
            //en caso de entrar en una página perfil de sin loguearse
        } else {
            ?>
            <link href="estilos/perfil_estilos_nologueado.css" rel="stylesheet" type="text/css"/>            
            <div id="cuerpo">
                <div id="logo">
                    <img src="sources/Logo Valinor 2.png" alt="logoValinor"/> 
                </div>   
                <div id="titulo"> VALINOR </div>
                <div id="denegado">
                    <img src='sources/alerta.png'>
                    <div id="nologueado"> <p> ACCESO DENEGADO: </p>
                        NO TIENES PERMISO PARA ENTRAR EN ESTA PÁGINA. </div>
                </div>
            </div>
            <?php
        }
        ?>
    </body>
</html>

