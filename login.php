<?php
require 'bbdd/bbdd.php';
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="estilos/login_estilos.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Quantico&display=swap" rel="stylesheet">
        <title>LOGIN</title>
    </head>
    <body>
        <div id="login">
            <h1>L O G I N</h1>
            <form method="POST">
                <input id="inp1" type="text" name="username" placeholder="Usuario" maxlength="10" required/><br><br><br>
                <input id="inp2" type="password" name="passwrd" placeholder="Contraseña" minlength="1" maxlength="10" required/><br><br><br><br>
                <input id="inp3" type="submit" name="login" value="ENTRAR">
            </form>
            <?php
            if (isset($_POST["login"])) {
                $username = $_POST["username"];
                $passwrd = $_POST["passwrd"];

                $resultado = verificarDatos($username, $passwrd);

                if ($resultado == true) {
                    $_SESSION["username"] = $username;
                    $_SESSION["tipo"] = recogerTipo($username);

                    switch ($_SESSION["tipo"]) {
                        case 0:
                            // Uso este script de redirect aquí para dirigirme al modificar con los datos de session activos, luego 
                            // cuando esté terminado redirigirá al perfil del usuario según su tipo.
                            echo '<script type="text/javascript">
                          window.location = "http://localhost/daw1mg6/perfil_admin.php"
                          </script>';
                            break;
                        case 1:
                            echo '<script type="text/javascript">
                          window.location = "http://localhost/daw1mg6/perfil_fan.php"
                          </script>';
                            break;
                        case 2:
                            echo '<script type="text/javascript">
                          window.location = "http://localhost/daw1mg6/perfil_local.php"
                          </script>';
                            break;
                        case 3:
                            echo '<script type="text/javascript">
                          window.location = "http://localhost/daw1mg6/perfil_musico.php"
                          </script>';
                            break;
                    }
                } else {
                    echo "<b>El nombre de usuario o la contraseña son incorrectos</b><br>";
                }
            }
            ?>
            <br>
            <a href="homepage.php">VOLVER</a><br>
        </div>
    </body>
</html>
