<?php
require_once 'bbdd/bbdd.php';
session_start();
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    $tipo = getTipo($username);
    ?>
    <html>
        <head>
            <meta charset="UTF-8">
            <link href="https://fonts.googleapis.com/css?family=Quantico&display=swap" rel="stylesheet">
            <link href="estilos/modificar_estilos.css" rel="stylesheet" type="text/css"/>
            <title>Modificar</title>
        </head>
        <body>
            <div id="modificarDatos">
                <h1> ACTUALIZAR PERFIL </h1>
                <?php
                $idUsuario = recoger_ID_Usuario($username);
                $datosUsuario = datosUsuario($idUsuario);
                ?>
                <form method="POST">   
                    <label>Email: </label>
                    <input type="email" name="email" placeholder="Ex: melkor@valinor.es" style="width: 200px" value="<?php echo $datosUsuario["email"] ?>"><br><br>
                    <label>Teléfono:</label>
                    <input type="tel" name="telefono" placeholder="Ex: 633383104" value="<?php echo $datosUsuario["telefono"] ?>" minlength="6" maxlength="9"><br><br>
                    <label>Ciudad: </label>
                    <select name="ciudad" value="<?php echo $datosUsuario["idciudad"] ?>">
                        <?php
                        $ciudades = listarCiudades();
                        while ($fila = mysqli_fetch_assoc($ciudades)) {
                            echo "<option";
                            if ($datosUsuario["idciudad"] == $fila["idciudad"]) {
                                echo " selected ";
                            }
                            echo " value='" . $fila["idciudad"] . "'>";
                            echo $fila["nombre"];
                            echo "</option>";
                        }
                        ?>
                    </select><br><br>
                    <label>Nombre: </label>
                    <input type="text" name="nombre" value="<?php echo $datosUsuario["nombre"] ?>"><br><br>
                    <?php
                    switch ($tipo) {
                        ////////////////////////////////////////////////////////////////
                        //                      MODIFICAR ADMIN
                        ////////////////////////////////////////////////////////////////
                        case 0:
                            ?>
                            <br><input class="passwrd" type="password" name="passwrd" placeholder="Introduce tu contraseña para actualizar tus datos" required><br><br>

                            <input class="submit" type="submit" name="modificar" value="MODIFICAR"><br>
                        </form>
                        <?php
                        break;
                    ////////////////////////////////////////////////////////////////
                    //                      MODIFICAR FAN
                    ////////////////////////////////////////////////////////////////
                    case 1:
                        $datosFan = datosFan($idUsuario);
                        ?>
                        <label>Apellidos: </label>
                        <input type="text" name="apellidos" placeholder="Apellidos" value="<?php echo $datosFan["apellidos"] ?>"><br><br>

                        <br><input class="passwrd" type="password" name="passwrd" placeholder="Introduce tu contraseña para actualizar tus datos" required><br><br>

                        <input class="submit" type="submit" name="modificar" value="MODIFICAR"><br>
                        </form>
                        <?php
                        break;
                    ////////////////////////////////////////////////////////////////
                    //                      MODIFICAR LOCAL
                    ////////////////////////////////////////////////////////////////
                    case 2:
                        $datosLocal = datosLocal($idUsuario);
                        ?>
                        <label>Dirección: </label>
                        <input type="text" name="direccion" placeholder="Dirección" style="width: 300px" value="<?php echo $datosLocal["ubicacion"] ?>"><br><br>
                        <label>Aforo: </label>
                        <input type="number" name="aforo" placeholder="Aforo" value="<?php echo $datosLocal["aforo"] ?>" min="1"><br><br>

                        <br><input class="passwrd" type="password" name="passwrd" placeholder="Introduce tu contraseña para actualizar tus datos" required><br><br>

                        <input class="submit" type="submit" name="modificar" value="MODIFICAR"><br>
                        </form>
                        <?php
                        break;
                    ////////////////////////////////////////////////////////////////
                    //                      MODIFICAR MUSICO
                    ////////////////////////////////////////////////////////////////
                    case 3:
                        $datosMusico = datosMusico($idUsuario);
                        ?>
                        <label>Web: </label>
                        <input type="text" name="web" style="width: 200px" value="<?php echo $datosMusico["web"] ?>"><br><br>
                        <label>Número de componentes: </label>
                        <input type="number" name="componentes" style="width: 50px" value="<?php echo $datosMusico["componentes"] ?>" min="1"><br><br>

                        <br><input class="passwrd" type="password" name="passwrd" placeholder="Introduce tu contraseña para actualizar tus datos" required><br><br>

                        <input class="submit" type="submit" name="modificar" value="MODIFICAR"><br>
                        </form>
                        <?php
                        break;
                }
                if (isset($_POST["modificar"])) {

                    $email = $_POST["email"];
                    $telefono = $_POST["telefono"];
                    $ciudad = $_POST["ciudad"];
                    $nombre = $_POST["nombre"];

                    $confirmPasswrd = $_POST["passwrd"];

                    if (verificarDatos($username, $confirmPasswrd) === true) {
                        switch ($tipo) {
                            case 0:
                                if (modificarAdmin($email, $telefono, $ciudad, $nombre, $idUsuario) === true) {
                                    echo "<b>Datos Actualizados</b><br><br>";
                                } else {
                                    echo $resultado;
                                }
                                break;
                            case 1:
                                $apellidos = $_POST["apellidos"];
                                modificarFan($email, $telefono, $ciudad, $nombre, $apellidos, $idUsuario);
                                break;
                            case 2:
                                $direccion = $_POST["direccion"];
                                $aforo = $_POST["aforo"];
                                modificarLocal($email, $telefono, $ciudad, $nombre, $direccion, $aforo, $idUsuario);
                                break;
                            case 3:
                                $web = $_POST["web"];
                                $componentes = $_POST["componentes"];
                                modificarMusico($email, $telefono, $ciudad, $nombre, $web, $componentes, $idUsuario);
                                break;
                        }
                    } else {
                        echo "La contraseña es incorrecta";
                    }
                }

                switch ($tipo) {
                    case 0:
                        ?>
                        <a href = "perfil_Admin.php">VOLVER</a><br>
                        <?php
                        break;
                    case 1:
                        ?>
                        <a href = "perfil_fan.php">VOLVER</a><br>
                        <?php
                        break;
                    case 2:
                        ?>
                        <a href = "perfil_local.php">VOLVER</a><br>
                        <?php
                        break;
                    case 3:
                        ?>
                        <a href = "perfil_musico.php">VOLVER</a><br>
                        <?php
                        break;
                }
                ?>
            </div>
        </body>
    </html>
    <?php
} else {
    echo "No tienes permiso para acceder a esta página";
}