<!DOCTYPE html>
<?php
require_once 'bbdd/bbdd.php';
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="script/registro.js" defer></script>
        <link href="estilos/registro.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <title></title>
    </head>
    <body>
        <div id="cont">


            <div id="global">
                <div id="buttons">
                    <button id="buttonFan">Fan</button><button 
                        id="buttonLocal">Local</button><button 
                        id="buttonMusico">Musico</button>
                </div>
                <div id="fan">
                    <form method="POST">
                        <input type="text" name="nombre" placeholder="Nombre" required><br>
                        <input type="text" name="apellido" placeholder="Apellidos" required><br>
                        <input type="text" name="nombreUsu" placeholder="Nombre de usuario" required><br>
                        <input type="password" name="passwd" required
                               placeholder="Contraseña" max="10"><br>
                        <input type="password" name="passwd2" required
                               placeholder="Repita la contraseña" max="10"><br>
                        <input type="email" name="email" placeholder="Email" required><br>
                        <input type="tel" name="tlf" placeholder="Teléfono" required><br>
                        <p>Ciudad</p> <select name="ciudad">
                            <?php
                            $ciudades = listarCiudades();
                            while ($row = mysqli_fetch_assoc($ciudades)) {
                                echo "<option value = '" . $row["idciudad"] . "'>";
                                echo $row["nombre"];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <p>Fecha de nacimiento</p> <input type="date" name="bday" required
                                                          style="margin-top: 0"><br>
                        <input class="enviar" type="submit" value="Registrarse" name="fan">
                    </form>
                </div>

                <div id="local" >
                    <form method="POST">
                        <input type="text" name="nombre" placeholder="Nombre del local" required><br>
                        <input type="text" name="nombreUsu" placeholder="Nombre de usuario" required><br>
                        <input type="password" name="passwd" required
                               placeholder="Contraseña" max="10"><br>
                        <input type="password" name="passwd2" required
                               placeholder="Repita la contraseña" max="10"><br>
                        <input type="email" name="email" placeholder="Email" required><br>
                        <input type="tel" name="tlf" placeholder="Teléfono" required><br>
                        <input type="number" name="aforo" min="0" placeholder="Aforo máximo" required><br>
                        <input type="text" name="direccion" placeholder="Direccion">
                        <p>Ciudad</p> <select name="ciudad">
                            <?php
                            $ciudades = listarCiudades();
                            while ($row = mysqli_fetch_assoc($ciudades)) {
                                echo "<option value = '" . $row["idciudad"] . "'>";
                                echo $row["nombre"];
                                echo "</option>";
                            }
                            ?>
                        </select>                        
                        <input class="enviar" type="submit" value="Registrarse" name="local">  
                    </form>
                </div>

                <div id="musico">
                    <form method="POST" action="">
                        <input type="text" name="nombre" placeholder="Nombre" required><br>
                        <input type="text" name="nombreUsu" placeholder="Nombre de usuario" required><br>
                        <input type="password" name="passwd" required
                               placeholder="Contraseña" max="10"><br>
                        <input type="password" name="passwd2" required
                               placeholder="Repita la contraseña" max="10"><br>
                        <input type="email" name="email" placeholder="Email" required><br>
                        <input type="tel" name="tlf" placeholder="Teléfono" required><br>
                        <input type="number" min="1" name="componentes" placeholder="Número de componentes"required>
                        <input type="text" placeholder="Web" name="web">
                        <p>Ciudad</p> <select name="ciudad">
                            <?php
                            $ciudades = listarCiudades();
                            while ($row = mysqli_fetch_assoc($ciudades)) {
                                echo "<option value = '" . $row["idciudad"] . "'>";
                                echo $row["nombre"];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <p>Genero</p> <select name="genero" required>
                            <?php
                            $resultado = selectAllGeneros();
                            while ($fila = mysqli_fetch_assoc($resultado)) {
                                echo "<option value='" . $fila["idgenero"] . "'>" . $fila["nombre"] . "</option>";
                            }
                            ?>
                        </select>

                        <input class="enviar" type="submit" value="Registrarse" name="musico">
                    </form>
                </div>
            </div>
        </div>

        <?php
        if (isset($_POST["fan"])) {
            $nombre = $_POST["nombre"];
            $apellido = $_POST["apellido"];
            $username = $_POST["nombreUsu"];
            $passwd = $_POST["passwd"];
            $passwd2 = $_POST["passwd2"];
            $email = $_POST["email"];
            $tlf = $_POST["tlf"];
            $idciudad = $_POST["ciudad"];
            $bday = $_POST["bday"];
            if ($passwd2 == $passwd) {
                if (!existeUsername($username)) {
                    if (!existeMail($email)) {
                        $hash = password_hash($passwd, PASSWORD_DEFAULT);
                        registroFan($username, $hash, $nombre, $apellido, $email, $tlf, $idciudad, $bday);
                        $_SESSION["username"] = $username;
                        echo '<script type="text/javascript">
                                window.location = "http://localhost/daw1mg6/perfil_fan.php"
                              </script>';
                    } else {
                        echo 'Ya existe una cuenta asociada a este email';
                    }
                } else {
                    echo 'Este nombre de usuario no está disponible';
                }
            } else {
                echo "Las contraseñas deben coincidir (control de errores con jQuery)";
            }
        }

        if (isset($_POST["local"])) {
            $nombre = $_POST["nombre"];
            $username = $_POST["nombreUsu"];
            $passwd = $_POST["passwd"];
            $passwd2 = $_POST["passwd2"];
            $email = $_POST["email"];
            $tlf = $_POST["tlf"];
            $idciudad = $_POST["ciudad"];
            $direccion = $_POST["direccion"];
            $aforo = $_POST["aforo"];
            if ($passwd2 == $passwd) {
                if (!existeUsername($username)) {
                    if (!existeMail($email)) {
                        $hash = password_hash($passwd, PASSWORD_DEFAULT);
                        registroLocal($username, $hash, $nombre, $email, $tlf, $idciudad, $direccion, $aforo);
                        $_SESSION["username"] = $username;
                        echo '<script type="text/javascript">
                                window.location = "http://localhost/daw1mg6/perfil_local.php"
                              </script>';
                    } else {
                        echo 'Ya existe una cuenta asociada a este email';
                    }
                } else {
                    echo 'Este nombre de usuario no está disponible';
                }
            } else {
                echo "Las contraseñas deben coincidir (control de errores con jQuery)";
            }
        }

        if (isset($_POST["musico"])) {
            $nombre = $_POST["nombre"];
            $username = $_POST["nombreUsu"];
            $passwd = $_POST["passwd"];
            $passwd2 = $_POST["passwd2"];
            $email = $_POST["email"];
            $tlf = $_POST["tlf"];
            $idciudad = $_POST["ciudad"];
            $genero = $_POST["genero"];
            $nComponentes = $_POST["componentes"];
            $web = $_POST["web"];
            if ($passwd2 == $passwd) {
                if (!existeUsername($username)) {
                    if (!existeMail($email)) {
                        $hash = password_hash($passwd, PASSWORD_DEFAULT);
                        $result = registroMusico($username, $hash, $nombre, $email, $tlf, $idciudad, $genero, $web, $nComponentes);
                        $_SESSION["username"] = $username;
                        echo '<script type="text/javascript">
                                window.location = "http://localhost/daw1mg6/perfil_musico.php"
                              </script>';
                    } else {
                        echo 'Ya existe una cuenta asociada a este email';
                    }
                } else {
                    echo 'Este nombre de usuario no está disponible';
                }
            } else {
                echo "Las contraseñas deben coincidir (control de errores con jQuery)";
            }
        }
        ?>
    </body>
</html>
