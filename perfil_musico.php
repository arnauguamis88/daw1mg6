<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="estilos/perfil_estilos.css" rel="stylesheet" type="text/css"/>
        <link href="estilos/estilosPropiosMusico.css" rel="stylesheet" type="text/css"/>
        <script src="script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="script/perfiles.js" type="text/javascript" defer></script>
        <title>Perfil</title>
    </head>
    <body>    
        <?php
        /* Iniciamos sesión, accedemos al fichero de bbdd, comprobamos si se ha logueado el usuario
          y guardamos los datos en caso afirmativo. */
        session_start();
        if (isset($_SESSION["username"])) {
            require_once 'bbdd/bbdd.php';
            $username = $_SESSION["username"];
            $idUsuario = recoger_ID_Usuario($username);
            $datosUsus = datosUsuario($idUsuario);
            $datosMusico = datosMusico($idUsuario);
            $datosGenero = generoMusico($idUsuario);
            $ciudad = tuCiudad($idUsuario);
            $fotoActual = recogerFoto($idUsuario);
            ?>

            <!--Parte superior de la página-->
            <header>               
                <!--Botón para abrir y cerrar el menu-->
                <div id="barraSuperiorBloque1">
                    <div id="capsulaBoton">
                        <div id="botonMenu"> 
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                        </div>
                    </div>
                </div>

                <!--Título y logo-->
                <div id="barraSuperiorBloque2">
                    <img src="sources/Logo Valinor 2.png" alt="logoValinor"/> 
                    <div id="titulo"> VALINOR </div>
                </div>

                <!--Botón para visualizar datos, apartado de foto con modificaciones incluidas al apretar sobre la foto-->
                <div id="barraSuperiorBloque3">
                    <div id="misDatos"> Mis datos </div>
                    <div id="barraSeparadora"></div>                  
                    <div id="fotoPerfil">               
                        <img src="sources/<?php echo $fotoActual ?>" alt="foto_de_perfil"/>
                    </div>
                </div>   

                <!--Menu-->
                <nav id="menu">
                    <ul>
                        <li> <a class = "apartado" id="propuestos" href="#"> Conciertos propuestos </a> </li>
                        <li> <a class = "apartado" id="aceptados" href="#"> Conciertos aceptados </a> </li>
                        <li> <a class = "apartado" href="#"> 3 </a> </li>
                        <li> <a class = "apartado" href="#"> 4 </a> </li>
                    </ul>
                </nav>
            </header>

            <!--Tabla con los datos del usu-->
            <div id="main"> <br>                     
                <div id="datos">                
                    <table border="1px black solid">
                        <tr> <td> <h1>Datos del perfil</h1> <!--<div id="cerrarMisDatos"> x </div> --> </td> </tr>
                        <tr> <td> <b> Nombre: </b> <?php echo $datosUsus["nombre"]; ?> </td> </tr>
                        <tr> <td> <b> Género: </b> <?php echo $datosGenero; ?> </td> </tr>
                        <tr> <td> <b> Nº Componentes: </b> <?php echo $datosMusico["componentes"]; ?> </td> </tr>
                        <tr> <td> <b> Ciudad: </b> <?php echo $ciudad; ?> </td> </tr>
                        <tr> <td> <b> Página web: </b> <?php echo $datosMusico["web"]; ?> </td> </tr>
                        <tr> <td> <b> Teléfono: </b> <?php echo $datosUsus["telefono"]; ?> </td> </tr>
                        <tr> <td> <b> E-Mail: </b> <?php echo $datosUsus["email"]; ?> </td> </tr>
                    </table>
                </div>
            </div><br>

            <!--Menu de modficiaciones al apretar la foto de perfil--> 
            <div id="usuario">
                <div id="triangulo"></div>
                <div id="opciones">
                    <ul>
                        <li><a href="modificarFoto.php">Cambiar foto de perfil</a></li>
                        <li><a href="modificarDatos.php">Modificar datos</a></li>
                        <li><a href="modificarPasswrd.php">Cambiar contraseña</a></li>
                        <li><a href="cerrar_session.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>

            <!--Listado de conciertos propuestos del género del músico-->
            <div id="altaBajaConciertos">
                <h1> Conciertos de <?php echo $datosGenero; ?> propuestos: </h1>
                <?php
                //comprobamos que haya conciertos de su genero musical
                if (conciertosPropuestosPorGenero($datosGenero)) {
                    //Recogemos todos los conciertos 
                    $conciertos = listadoConciertosPropuestos();
                    ?>
                    <!--Creamos una tabla para el listado-->
                    <table border = "1px black">
                        <tr> <th> Nombre </th> <th> Fecha </th> <th> Hora </th> <th> Presupuesto </th> <th> Estado de la solicitud </th> </tr>
                        <?php
                        //los pasamos a un array asociativo
                        while ($fila = mysqli_fetch_assoc($conciertos)) {
                            //los limitamos a los que sean del mismo género del músico
                            if (devolverGenero($fila["idgenero"]) == $datosGenero) {
                                echo '<tr>';
                                echo "<td class='datosBasicos'>" . $fila["nombre"] . '</td>';
                                echo "<td class='datosBasicos'>" . $fila["fecha"] . '</td>';
                                echo "<td class='datosBasicos'>" . $fila["hora"] . '</td>';
                                echo "<td class='datosBasicos'>" . $fila["presupuesto"] . '€' . '</td>';
                                //añadimos la función para saber si se ha hecho una solicitud
                                $existeSolicitud = existeSolicitud($idUsuario, $fila["idconcierto"]);
                                //añadimos la función para extraer datos concretos
                                $row = estadoPropuestas($idUsuario, $fila["idconcierto"]);
                                //si no hay solicitudes en la tabla de propuestas o el usuario no ha hecho una solicitud para el concierto
                                if (existenPropuestas($idUsuario) == false || $existeSolicitud == false) {
                                    echo "<td class='estadoSol' id='noEnviada'> Sin solicitar </td>";
                                    ?>
                                    <td>
                                        <!--Formulario para enviar la solicitud-->
                                        <form method = "POST">
                                            <input type = "hidden" name = "idconcierto" value = "<?php echo $fila["idconcierto"]; ?> ">
                                            <input type = "hidden" name = "nombreconcierto" value = "<?php echo $fila["nombre"]; ?> ">
                                            <input type = "submit" name = "apuntarse" value = "Inscribirse">
                                        </form>
                                    </td>
                                    <?php
                                    //si hay solicitudes en la tabla de propuestas
                                } else {
                                    //en caso de que se haya hecho una solicitud
                                    if ($existeSolicitud == true) {
                                        if ($row["estadoPropuesta"] == 1) {
                                            //aviso para el músico de que su solicitud está pendiente
                                            if ($row["idconcierto"] == $fila["idconcierto"]) {
                                                echo "<td class='estadoSol' id='solPendiente'> Pendiente </td>";
                                                ?>                                                                                    
                                                <td>
                                                    <!--Formulario para cancelar la solicitud-->
                                                    <form method="POST">
                                                        <input type = "hidden" name = "idconcierto" value = "<?php echo $fila["idconcierto"]; ?> ">
                                                        <input type = "hidden" name = "nombreconcierto" value = "<?php echo $fila["nombre"]; ?> ">
                                                        <input type ="submit" name ="desapuntarse" value ="Desapuntarse">
                                                    </form>                               
                                                </td>                                           
                                                <?php
                                            }
                                        }
                                    }
                                }
                                echo '</tr>';
                            }
                        }
                        ?>
                    </table>
                    <?php
                    // si apreta a inscribirse guardará las variables ocultas y agregara su solictud a la tabla de propuestas, con su correspondiente feedback
                    if (isset($_POST["apuntarse"])) {
                        $idconcierto = $_POST["idconcierto"];
                        $nombreConcierto = $_POST["nombreconcierto"];
                        $resultado = apuntarMusico($idUsuario, $idconcierto);
                        echo "<div class='respuesta'> Solicitud enviada para: $nombreConcierto. </div>";
                        ?>
                        <meta http-equiv=refresh content=2;URL=perfil_musico.php>
                        <?php
                    }
                    // si apreta a desapuntarse guardará las variables ocultas y eliminará su solictud de la tabla de propuestas, con su correspondiente feedback
                    if (isset($_POST["desapuntarse"])) {
                        $idconcierto = $_POST["idconcierto"];
                        $nombreConcierto = $_POST["nombreconcierto"];
                        $resultado = desapuntarMusico($idUsuario, $idconcierto);
                        echo "<div class='respuesta'> Solicitud cancelada para: $nombreConcierto. </div>";
                        ?>
                        <meta http-equiv=refresh content=2;URL=perfil_musico.php>
                        <?php
                    }
                    //en caso de que no haya conciertos de su género
                } else {
                    echo "No hay conciertos de $datosGenero propuestos.";
                }
                ?>  
            </div>        

            <!--Listado de conciertos aceptados del género del músico-->
            <div id="listadoConciertosAceptados">             
                <h1> Conciertos de <?php echo $datosGenero; ?> aceptados: </h1>
                <?php
                //comprobamos que haya conciertos aceptados del genero del músico
                if (conciertosAceptadosPorGenero($datosGenero)) {
                    //Recogemos todos los conciertos aceptados
                    $conciertos = listadoConciertosAceptados();
                    ?>
                    <!--Creamos una tabla para el listado-->
                    <table border = "1px black">
                        <tr> <th> Nombre </th> <th> Fecha </th> <th> Hora </th> <th> Presupuesto </th> <th> Estado de la solicitud </th> </tr>
                        <?php
                        //pasamos los conciertos aceptados a un array asociativo
                        while ($fila = mysqli_fetch_assoc($conciertos)) {
                            //limitamos los conciertos a los que sean del mismo género del músico
                            if (devolverGenero($fila["idgenero"]) == $datosGenero) {
                                echo '<tr>';
                                echo "<td class='datosBasicos'>" . $fila["nombre"] . '</td>';
                                echo "<td class='datosBasicos'>" . $fila["fecha"] . '</td>';
                                echo "<td class='datosBasicos'>" . $fila["hora"] . '</td>';
                                echo "<td class='datosBasicos'>" . $fila["presupuesto"] . '€' . '</td>';
                                if ($fila["idmusico"] == $idUsuario) {
                                    echo "<td class='estadoSol' id='solAceptada'> Aceptada </td>";
                                    // aviso para el músico si ha sido rechazado
                                } else if ($fila["idmusico"] != $idUsuario) {
                                    echo "<td class='estadoSol' id='solRechazada'> Rechazada </td>";
                                }
                            }
                        }
                        echo '</tr>';
                        ?>
                    </table>
                    <?php
                    //en caso de que no haya conciertos aceptados de su género
                } else {
                    echo "No hay conciertos de $datosGenero aceptados.";
                }
                ?>  
            </div>

            <?php
            //en caso de entrar en una página perfil de sin loguearse
        } else {
            ?>
            <link href="estilos/perfil_estilos_nologueado.css" rel="stylesheet" type="text/css"/>            
            <div id="cuerpo">
                <div id="logo">
                    <img src="sources/Logo Valinor 2.png" alt="logoValinor"/> 
                </div>   
                <div id="titulo"> VALINOR </div>
                <div id="denegado">
                    <img src='sources/alerta.png'>
                    <div id="nologueado"> <p> ACCESO DENEGADO: </p>
                        NO TIENES PERMISO PARA ENTRAR EN ESTA PÁGINA. </div>
                </div>
            </div>
            <?php
        }
        ?>
    </body>
</html>
