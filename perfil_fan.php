<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="estilos/perfil_estilos.css" rel="stylesheet" type="text/css"/>
        <link href="estilos/estilosPropiosFan.css" rel="stylesheet" type="text/css"/>
        <script src="script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="script/perfiles.js" type="text/javascript" defer></script>
        <title>Perfil</title>
    </head>
    <body>       
        <?php
        /* Iniciamos sesión, accedemos al fichero de bbdd, comprobamos si se ha logueado el usuario
          y guardamos los datos en caso afirmativo. */
        session_start();
        if (isset($_SESSION["username"])) {
            require_once 'bbdd/bbdd.php';
            $username = $_SESSION["username"];
            $idUsuario = recoger_ID_Usuario($username);
            $datosUsus = datosUsuario($idUsuario);
            $datosFan = datosFan($idUsuario);
            $ciudad = tuCiudad($idUsuario);
            $fotoActual = recogerFoto($idUsuario);
            ?>
            <!--Parte superior de la página-->
            <header>               
                <!--Botón para abrir y cerrar el menu-->
                <div id="barraSuperiorBloque1">
                    <div id="capsulaBoton">
                        <div id="botonMenu"> 
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                            <span class="spanes"></span>
                        </div>
                    </div>
                </div>

                <!--Título y logo-->
                <div id="barraSuperiorBloque2">
                    <img src="sources/Logo Valinor 2.png" alt="logoValinor"/> 
                    <div id="titulo"> VALINOR </div>
                </div>

                <!--Botón para visualizar datos, apartado de foto con modificaciones incluidas al apretar sobre la foto-->
                <div id="barraSuperiorBloque3">
                    <div id="misDatos"> Mis datos </div>
                    <div id="barraSeparadora"></div>                  
                    <div id="fotoPerfil">               
                        <img src="sources/<?php echo $fotoActual ?>" alt="foto_de_perfil"/>
                    </div>
                </div>   

                <!--Menu-->
                <nav id="menu">
                    <ul>
                        <li> <a class = "apartado" href="#"> 1 </a> </li>
                        <li> <a class = "apartado" href="#"> 2 </a> </li>
                        <li> <a class = "apartado" href="#"> 3 </a> </li>
                        <li> <a class = "apartado" href="#"> 4 </a> </li>
                    </ul>
                </nav>
            </header>

            <!--Tabla con los datos del usu-->
            <div id="main">                
                <div id="datos">
                    <table border="1px black solid">
                        <tr> <td> <h1>Datos del perfil</h1> <!--<div id="cerrarMisDatos"> x </div> --> </td> </tr>
                        <tr> <td> <b> Nombre: </b> <?php echo $datosUsus["nombre"]; ?> </td> </tr>
                        <tr> <td> <b> Apellidos: </b> <?php echo $datosFan["apellidos"]; ?> </td> </tr>
                        <tr> <td> <b> Fecha de nacimiento: </b> <?php echo $datosFan["nacimiento"] ?> </td> </tr>
                        <tr> <td> <b> E-Mail: </b> <?php echo $datosUsus["email"]; ?> </td> </tr>
                        <tr> <td> <b> Teléfono: </b> <?php echo $datosUsus["telefono"]; ?> </td> </tr>
                        <tr> <td> <b> Fecha de alta: </b> <?php echo $datosFan["fechaalta"]; ?> </td> </tr>
                        <tr> <td> <b> Ciudad: </b> <?php echo $ciudad; ?> </td> </tr>
                    </table>
                </div>
            </div><br>

            <!--Menu de modficiaciones al apretar la foto de perfil--> 
            <div id="usuario">
                <div id="triangulo"></div>
                <div id="opciones">
                    <ul>
                        <li><a href="modificarFoto.php">Cambiar foto de perfil</a></li>
                        <li><a href="modificarDatos.php">Modificar datos</a></li>
                        <li><a href="modificarPasswrd.php">Cambiar contraseña</a></li>
                        <li><a href="cerrar_session.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>

            <?php
            //en caso de entrar en una página perfil de sin loguearse
        } else {
            ?>
            <link href="estilos/perfil_estilos_nologueado.css" rel="stylesheet" type="text/css"/>            
            <div id="cuerpo">
                <div id="logo">
                    <img src="sources/Logo Valinor 2.png" alt="logoValinor"/> 
                </div>   
                <div id="titulo"> VALINOR </div>
                <div id="denegado">
                    <img src='sources/alerta.png'>
                    <div id="nologueado"> <p> ACCESO DENEGADO: </p>
                        NO TIENES PERMISO PARA ENTRAR EN ESTA PÁGINA. </div>
                </div>
            </div>
            <?php
        }
        ?>
    </body>
</html>
