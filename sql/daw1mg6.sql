CREATE DATABASE  IF NOT EXISTS `daw1mg6` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `daw1mg6`;
-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: daw1mg6
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `idciudad` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  PRIMARY KEY (`idciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'A Coruña','A Coruña'),(2,'Alava','Alava'),(3,'Albacete','Albacete'),(4,'Alicante','Alicante'),(5,'Almería','Almería'),(6,'Asturias','Asturias'),(7,'Avila','Avila'),(8,'Badajoz','Badajoz'),(9,'Baleares','Baleares'),(10,'Barcelona','Barcelona'),(11,'Bizkaia','Bizkaia'),(12,'Burgos','Burgos'),(13,'Cáceres','Cáceres'),(14,'Cádiz','Cádiz'),(15,'Cantabria','Cantabria'),(16,'Castellón','Castellón'),(17,'Ceuta','Ceuta'),(18,'Ciudad Real','Ciudad Real'),(19,'Córdoba','Córdoba'),(20,'Cuenca','Cuenca'),(21,'Guipuzkoa','Guipuzkoa'),(22,'Girona','Girona'),(23,'Granada','Granada'),(24,'Guadalajara','Guadalajara'),(25,'Huelva','Huelva'),(26,'Huesca','Huesca'),(27,'Jaén','Jaén'),(28,'La Rioja','La Rioja'),(29,'Las Palmas','Las Palmas'),(30,'León','León'),(31,'Lugo','Lugo'),(32,'Lleida','Lleida'),(33,'Madrid','Madrid'),(34,'Málaga','Málaga'),(35,'Melilla','Melilla'),(36,'Murcia','Murcia'),(37,'Navarra','Navarra'),(38,'Ourense','Ourense'),(39,'Palencia','Palencia'),(40,'Pontevedra','Pontevedra'),(41,'Salamanca','Salamanca'),(42,'Segovia','Segovia'),(43,'Sevilla','Sevilla'),(44,'Soria','Soria'),(45,'Tarragona','Tarragona'),(46,'Tenerife','Tenerife'),(47,'Teruel','Teruel'),(48,'Toledo','Toledo'),(49,'Valencia','Valencia'),(50,'Valladolid','Valladolid'),(51,'Zamora','Zamora'),(52,'Zaragoza','Zaragoza');
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `concierto`
--

DROP TABLE IF EXISTS `concierto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concierto` (
  `idconcierto` int(11) NOT NULL AUTO_INCREMENT,
  `idmusico` int(11) DEFAULT NULL,
  `idlocal` int(11) NOT NULL,
  `idgenero` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `presupuesto` int(255) NOT NULL,
  `entrada` int(100) NOT NULL,
  `estadoConcierto` int(5) NOT NULL,
  PRIMARY KEY (`idconcierto`),
  UNIQUE KEY `nombre_concierto` (`nombre`) USING HASH,
  KEY `FK_idmusico_concierto` (`idmusico`),
  KEY `FK_idlocal_concierto` (`idlocal`),
  KEY `FK_idgenero_concierto` (`idgenero`),
  CONSTRAINT `FK_idgenero_concierto` FOREIGN KEY (`idgenero`) REFERENCES `genero` (`idgenero`),
  CONSTRAINT `FK_idlocal_concierto` FOREIGN KEY (`idlocal`) REFERENCES `lokal` (`idlocal`),
  CONSTRAINT `FK_idmusico_concierto` FOREIGN KEY (`idmusico`) REFERENCES `musico` (`idmusico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `concierto`
--

LOCK TABLES `concierto` WRITE;
/*!40000 ALTER TABLE `concierto` DISABLE KEYS */;
INSERT INTO `concierto` VALUES (1,9,3,3,'Resurrection Fest','2020-06-02','10:00:00',1500000,50,2),(3,7,3,3,'Rock Fest','2021-10-12','12:11:00',3343233,10,2),(4,6,3,1,'Hipnotic','2020-12-23','01:12:00',34234323,19,2);
/*!40000 ALTER TABLE `concierto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fan`
--

DROP TABLE IF EXISTS `fan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fan` (
  `idfan` int(11) NOT NULL,
  `apellidos` varchar(10) NOT NULL,
  `fechaalta` date NOT NULL,
  `nacimiento` date NOT NULL,
  PRIMARY KEY (`idfan`),
  CONSTRAINT `FK_idfan` FOREIGN KEY (`idfan`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fan`
--

LOCK TABLES `fan` WRITE;
/*!40000 ALTER TABLE `fan` DISABLE KEYS */;
INSERT INTO `fan` VALUES (2,'fas','2020-06-23','1996-10-29');
/*!40000 ALTER TABLE `fan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idgenero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idgenero`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,'Hip Hop'),(2,'Heavy Metal'),(3,'Rock'),(4,'Pop'),(5,'Dubstep'),(6,'Electronica'),(7,'House'),(8,'Ska'),(9,'Reggea'),(10,'Nightcore'),(11,'Hardcore'),(12,'Frenchcore'),(13,'Punk'),(14,'ThrashMetal'),(15,'Trap');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lokal`
--

DROP TABLE IF EXISTS `lokal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lokal` (
  `idlocal` int(11) NOT NULL,
  `aforo` int(20) NOT NULL,
  `ubicacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idlocal`),
  CONSTRAINT `FK_idlocal` FOREIGN KEY (`idlocal`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lokal`
--

LOCK TABLES `lokal` WRITE;
/*!40000 ALTER TABLE `lokal` DISABLE KEYS */;
INSERT INTO `lokal` VALUES (3,500,'Josep Maria de Segarra n14 Bajos');
/*!40000 ALTER TABLE `lokal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musico`
--

DROP TABLE IF EXISTS `musico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musico` (
  `idmusico` int(11) NOT NULL,
  `idgenero` int(11) NOT NULL,
  `web` varchar(200) DEFAULT NULL,
  `componentes` int(5) NOT NULL,
  PRIMARY KEY (`idmusico`),
  KEY `FK_idgenero` (`idgenero`),
  CONSTRAINT `FK_idgenero` FOREIGN KEY (`idgenero`) REFERENCES `genero` (`idgenero`),
  CONSTRAINT `FK_idmusico` FOREIGN KEY (`idmusico`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musico`
--

LOCK TABLES `musico` WRITE;
/*!40000 ALTER TABLE `musico` DISABLE KEYS */;
INSERT INTO `musico` VALUES (4,1,'www.stucom.com',6),(6,1,'www.nachscratch.es',2),(7,3,'www.joesatriani.com',4),(8,3,'www.rosendo.es',4),(9,2,'www.avengedsevenfold.com',5);
/*!40000 ALTER TABLE `musico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `propuesta`
--

DROP TABLE IF EXISTS `propuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propuesta` (
  `idmusico` int(11) DEFAULT NULL,
  `idconcierto` int(11) DEFAULT NULL,
  `estadoPropuesta` int(5) NOT NULL,
  KEY `FK_idmusico_propuesta` (`idmusico`),
  KEY `FK_idconcierto_propuesta` (`idconcierto`),
  CONSTRAINT `FK_idconcierto_propuesta` FOREIGN KEY (`idconcierto`) REFERENCES `concierto` (`idconcierto`),
  CONSTRAINT `FK_idmusico_propuesta` FOREIGN KEY (`idmusico`) REFERENCES `musico` (`idmusico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `propuesta`
--

LOCK TABLES `propuesta` WRITE;
/*!40000 ALTER TABLE `propuesta` DISABLE KEYS */;
INSERT INTO `propuesta` VALUES (7,3,2),(8,3,2),(7,1,2),(4,4,2);
/*!40000 ALTER TABLE `propuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `passwrd` varchar(255) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `telefono` varchar(50) NOT NULL,
  `idciudad` int(11) NOT NULL,
  `tipo` int(5) NOT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `username_index` (`username`) USING HASH,
  UNIQUE KEY `email_index` (`email`) USING HASH,
  KEY `FK_idciudad` (`idciudad`),
  CONSTRAINT `FK_idciudad` FOREIGN KEY (`idciudad`) REFERENCES `ciudad` (`idciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','$2y$10$gEACSL8wtMRvW0fc3khN.OTRGDfCioOftyjDPl2vPioo2efT4wBly','Admin','administrador1@admin.com','Logo Valinor 2.png','',3,0),(2,'fan','$2y$10$FeOk604GvUV6R/oJNNO8DOoOLm/9.e.MODszZ4NX2OGSwDUl6svxu','fan','fan@fan.com','Juan_cuesta.png','',52,1),(3,'local','$2y$10$f674cpFggL8P2HvAaPxvSe6Lf2/Ji8KKRjQI3RuFDnc2OFA06Q0E6','BÃ³veda','local@local.com','Logo Valinor 2.png','',3,2),(4,'musico','$2y$10$EHk1IhLxjrENnLJrlQAUE.cl/UwcWvmtJFvbJ59RK6/xjiX9ODprC','Musico','musico@musico.com','Logo Valinor 2.png','',7,3),(6,'nach','$2y$10$KIw3t5I0JF0yqvr5XEqqOOE55.RfMr6STGjrHDxCR8OtlxwLqV.0W','Nach','nach@nach.com','nach.jpg','',4,3),(7,'joe','$2y$10$jheovu7WYpVkRW4bfZD8sOgWugq7zV3Ciu2xWzZ8kJAU2FmQMMGQS','Joe Satriani','joesatriani@gmail.com','joesatriani.jpg','',52,3),(8,'rosendo','$2y$10$inAe2Iniu2Uuh9ncxIDKkOSIKMp1dDmLHGkcIWV9OlTSyqhP63atK','Rosendo','rosendo@gmail.com','Logo Valinor 2.png','',34,3),(9,'a7x','$2y$10$vyJTvlwPlI7Qa9dEuqEWXOveLGfoT6v1vsqe4.K7sN3vZj5zl1EJu','Avenged Sevenfold','a7x@gmail.com','avenged1.jpg','',10,3);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votar_concierto`
--

DROP TABLE IF EXISTS `votar_concierto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votar_concierto` (
  `idfan` int(11) DEFAULT NULL,
  `idconcierto` int(11) DEFAULT NULL,
  `puntuacion` int(255) NOT NULL,
  KEY `FK_idfan_votarC` (`idfan`),
  KEY `FK_idconcierto_votarC` (`idconcierto`),
  CONSTRAINT `FK_idconcierto_votarC` FOREIGN KEY (`idconcierto`) REFERENCES `concierto` (`idconcierto`),
  CONSTRAINT `FK_idfan_votarC` FOREIGN KEY (`idfan`) REFERENCES `fan` (`idfan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votar_concierto`
--

LOCK TABLES `votar_concierto` WRITE;
/*!40000 ALTER TABLE `votar_concierto` DISABLE KEYS */;
/*!40000 ALTER TABLE `votar_concierto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votar_musico`
--

DROP TABLE IF EXISTS `votar_musico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votar_musico` (
  `idfan` int(11) DEFAULT NULL,
  `idmusico` int(11) DEFAULT NULL,
  `puntuacion` int(255) NOT NULL,
  KEY `FK_idfan_votarM` (`idfan`),
  KEY `FK_idmusico_votarM` (`idmusico`),
  CONSTRAINT `FK_idfan_votarM` FOREIGN KEY (`idfan`) REFERENCES `fan` (`idfan`),
  CONSTRAINT `FK_idmusico_votarM` FOREIGN KEY (`idmusico`) REFERENCES `musico` (`idmusico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votar_musico`
--

LOCK TABLES `votar_musico` WRITE;
/*!40000 ALTER TABLE `votar_musico` DISABLE KEYS */;
/*!40000 ALTER TABLE `votar_musico` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-06 13:27:15
