<?php
require 'bbdd/bbdd.php';
session_start();
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    $tipo = getTipo($username);
    $idUsuario = recoger_ID_Usuario($username);
    $fotoActual = recogerFoto($idUsuario);
    ?>
    <html>
        <head>
            <meta charset="UTF-8">
            <link href="https://fonts.googleapis.com/css?family=Quantico&display=swap" rel="stylesheet">
            <link href="estilos/modificar_estilos.css" rel="stylesheet" type="text/css"/>
            <title>Modificar</title>
        </head>
        <body>
            <div id="modificarDatos">
                <h1> CAMBIAR FOTO </h1><br>
                <form method="POST">
                    <?php
                    if($fotoActual == null){
                        $fotoActual = "Logo Valinor 2.png";
                    }
                    ?>
                    <img id="fotoActual" src="sources/<?php echo $fotoActual ?>" alt=""/>
                    <input id="subirFoto" name="foto" type="file" accept="image/png, image/jpeg" required><br><br><br>
                    <input class="submit" type="submit" name="cambiar" value="Cambiar"><br>
                </form>
                <?php
                if (isset($_POST["cambiar"])) {
                    $fotoNueva = $_POST["foto"];
                    if (cambiarFoto($idUsuario, $fotoNueva)) {
                        echo "Foto Actualizada<br>";
                    }
                }
                ?>
        </body>
        <?php
        switch ($tipo) {
            case 0:
                ?>
                <a href = "perfil_Admin.php">VOLVER</a><br>
                <?php
                break;
            case 1:
                ?>
                <a href = "perfil_fan.php">VOLVER</a><br>
                <?php
                break;
            case 2:
                ?>
                <a href = "perfil_local.php">VOLVER</a><br>
                <?php
                break;
            case 3:
                ?>
                <a href = "perfil_musico.php">VOLVER</a><br>
                <?php
                break;
        }
        ?>
    </div>
    </html>
    <?php
} else {
    echo "No tienes permiso para acceder a esta página";
}
