<?php
require_once 'bbdd/bbdd.php';
?>
<img id="logoIntro" src="sources/Logo Valinor 2.png"/>
<div class="easterEgg">
    El reino de Valinor «Tierra de los Valar» es un territorio ficticio que aparece en las obras de J. R. R. Tolkien, principalmente en El Silmarillion. Es el reino que fundaron los Valar en el continente de Aman, luego del devastamiento perpetrado por Melkor sobre el que tuvieran inicialmente en la Tierra Media, Almaren. A Aman se podía llegar en barco desde la Tierra Media, atravesando el Gran Mar, pero tras la rebelión del rey Ar-Phârazon de Númenor en 3319 S. E., Valinor y las tierras de Aman fueron separadas de los círculos de la Tierra, y desde entonces pasaron a ser sólo alcanzables para los Elfos. Al final de la Tercera Edad, Elrond y otros que habían sido portadores de los Anillos de Poder, incluyendo a Frodo Bolsón, su tío Bilbo Bolsón, la elfa Galadriel y Gandalf, partieron de la Tierra Media para vivir en Tol Eressëa, muy cerca de Valinor, durante el resto de sus vidas. Más tarde viajarían también hacia Valinor Samsagaz Gamyi, Legolas y Gimli. Este último sería el único enano en pisar Valinor.
    <br><br><div id="leido">LEÍDO</div>
</div>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="estilos/homepage_estilos.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Oxanium&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Quantico&display=swap" rel="stylesheet">
        <script src="script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="script/homepage.js" type="text/javascript"></script>
        <title>Valinor - Página de inicio</title>
    </head>
    <body>
        <header>
            <div id="logo">
                <img id="logoImg" src="sources/Logo Valinor 2.png" alt="logoValinor" height="110px" width="110px "/>
            </div>
            <h1 id="titulo"> VALINOR </h1>
            <div id="botones">
                <input type="button" id="boton-login" onclick="location.href = 'login.php';" value="LOGIN" /><br>
                <input type="submit" id="boton-registro" onclick="location.href = 'registro.php';" value="REGISTRO" />
            </div>
        </header>
        <div id="main">
            <aside>
                <ul>
                    <li>
                        <a>TOP CONCIERTOS</a>
                    </li>
                    <li>
                        <a>TOP MÚSICOS</a>
                    </li>
                    <li>
                        <a>LOCALES MÁS MOLONES</a>
                    </li>
                    <li>
                        <a>STUKEMON</a>
                    </li>
                    <li>
                        <a>PEORES CONCIERTOS</a>
                    </li>
                    <li>
                        <a>CIUDADES MÁS CAÑERAS</a>
                    </li>
                    <li>
                        <a>NO SÉ QUE MÁS PONER</a>
                    </li>

                    <li>
                        <a href="https://media.istockphoto.com/vectors/chinese-dragon-in-the-sky-vector-id667827900">ALEC ES UN DRAGON</a>
                    </li>
                </ul>
            </aside>
            <div id="lista">     
                <section class="concierto">
                    <h2>Lista de locales</h2>
                    <?php
                    $result = listaLocales();
                    $row = mysqli_fetch_assoc($result);
                    echo '<table><tr>';
                    foreach ($row as $value => $key) {
                        echo '<th>' . $value . '</th>';
                    }
                    echo '</tr>';
                    do {
                        echo '<tr>';
                        foreach ($row as $value) {
                            echo '<td>' . $value . '</td>';
                        }
                        echo '</tr>';
                    } while ($row = mysqli_fetch_assoc($result));
                    echo '</table>';
                    ?>
                </section>

                <section class="concierto">
                    <?php
                    $musicos = selectAllMusicos();
                    if (mysqli_num_rows($musicos) == 0) {
                        echo "No hay músicos registrados. ";
                    } else {
                        ?>
                        <table>
                            <tr> <th> Nombre </th> <th> Género musical </th> <th> E-mail </th> <th> Página web </th> <th> Teléfono de contacto </th> </tr>
                            <?php
                            $lista = listaDeMusicos();
                            while ($fila = mysqli_fetch_assoc($lista)) {
                                echo '<tr>';
                                echo '<td>' . $fila["nombre"] . '</td>';
                                echo '<td>' . $fila["genero"] . '</td>';
                                echo '<td>' . $fila["email"] . '</td>';
                                echo '<td>' . $fila["web"] . '</td>';
                                echo '<td>' . $fila["telefono"] . '</td>';
                                echo '<tr>';
                            }
                        }
                        ?>                       
                    </table>
                </section>


                <section class="concierto">
                    <?php
                    $conciertos = selectAllConciertos();
                    if (mysqli_num_rows($conciertos) == 0) {
                        echo "No hay conciertos registrados. ";
                    } else {
                        ?>
                        <table>
                            <h2> Lista de conciertos: </h2>
                            <tr> <th> Nombre </th> <th> Músico </th> <th> Género musical </th> <th> Fecha </th> <th> Hora </th> <th> Precio de entrada </th> </tr>
                            <?php
                            $lista = listaDeConciertos();
                            while ($fila = mysqli_fetch_assoc($lista)) {
                                echo '<tr>';
                                echo '<td>' . $fila["nombre"] . '</td>';
                                echo '<td>' . $fila["musico"] . '</td>';
                                echo '<td>' . $fila["genero"] . '</td>';
                                echo '<td>' . $fila["fecha"] . '</td>';
                                echo '<td>' . $fila["hora"] . '</td>';
                                echo '<td>' . $fila["entrada"] . "€" . '</td>';
                                echo '<tr>';
                            }
                        }
                        ?>                       
                    </table>
                </section>


                <section class="concierto">
                    <?php
                    $conciertos = selectConciertosVotados();
                    if (mysqli_num_rows($conciertos) == 0) {
                        echo "No hay conciertos puntuados. ";
                    } else {
                        ?>
                        <table>
                            <h2> TOP 5 de conciertos: </h2>
                            <tr> <th> Ranking </th> <th> Nombre </th> <th> Músico </th> <th> Género musical </th> <th> Nº de votos  </th> </tr>
                            <?php
                            $lista = topConciertos();
                            $contador = 1;
                            while ($fila = mysqli_fetch_assoc($lista)) {
                                echo '<tr>';
                                echo '<td>' . $contador . '</td>';
                                echo '<td>' . $fila["nombre"] . '</td>';
                                echo '<td>' . $fila["musico"] . '</td>';
                                echo '<td>' . $fila["genero"] . '</td>';
                                echo '<td>' . $fila["puntuacion"] . '</td>';
                                echo '<tr>';
                                $contador++;
                            }
                        }
                        ?>                       
                    </table>


                </section>

                <section class="concierto">
                    <?php
                    $conciertos = selectMusicosVotados();
                    if (mysqli_num_rows($conciertos) == 0) {
                        echo "No hay músicos puntuados. ";
                    } else {
                        ?>
                        <table>
                            <h2> TOP 5 de músicos: </h2>
                            <tr> <th> Ranking </th> <th> Nombre </th> <th> Género musical </th> <th> Nº de votos </th> </tr>
                            <?php
                            $lista = topMusicos();
                            $contador = 1;
                            while ($fila = mysqli_fetch_assoc($lista)) {
                                echo '<tr>';
                                echo '<td>' . $contador . '</td>';
                                echo '<td>' . $fila["musico"] . '</td>';
                                echo '<td>' . $fila["genero"] . '</td>';
                                echo '<td>' . $fila["puntuacion"] . '</td>';
                                echo '<tr>';
                                $contador++;
                            }
                        }
                        ?>                       
                    </table>
                </section>
            </div>
            
        </div>
    </body>
</html>
