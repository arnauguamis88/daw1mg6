<?php
require_once '../bbdd/bbdd.php';
session_start();
?>


<div id="altaBajaConciertos">
    <?php
    $username = $_SESSION["username"];
    $idUsuario = recoger_ID_Usuario($username);
    $datosGenero = generoMusico($idUsuario);
    ?>
    <h1>Propuestas de <?php echo $datosGenero; ?> </h1>
    <?php
    //comprobamos que haya conciertos de su genero musical
    if (conciertosPropuestosPorGenero($datosGenero)) {
        //Recogemos todos los conciertos 
        $conciertos = listadoConciertosPropuestos();
        ?>
        <!--Creamos una tabla para el listado-->
        <table border = "1px black">
            <tr> <th style="border-radius: 5px 0px 0px 0px;"> Nombre </th> <th> Fecha </th> <th> Hora </th> <th> Presupuesto </th><th style="border-radius: 0px 5px 0px 0px;"> Apuntarse </th></tr>
            <?php
            //los pasamos a un array asociativo
            while ($fila = mysqli_fetch_assoc($conciertos)) {
                //los limitamos a los que sean del mismo género del músico
                if (devolverGenero($fila["idgenero"]) == $datosGenero) {
                    echo '<tr>';
                    echo "<td class='datosBasicos'>" . $fila["nombre"] . '</td>';
                    echo "<td class='datosBasicos'>" . $fila["fecha"] . '</td>';
                    echo "<td class='datosBasicos'>" . $fila["hora"] . '</td>';
                    echo "<td class='datosBasicos'>" . $fila["presupuesto"] . '€' . '</td>';
                    //añadimos la función para saber si se ha hecho una solicitud
                    $existeSolicitud = existeSolicitud($idUsuario, $fila["idconcierto"]);
                    //añadimos la función para extraer datos concretos
                    $row = estadoPropuestas($idUsuario, $fila["idconcierto"]);
                    //si no hay solicitudes en la tabla de propuestas o el usuario no ha hecho una solicitud para el concierto
                    if (existenPropuestas($idUsuario) == false || $existeSolicitud == false) {
                        
                        ?>
                        <td>
                            <!--Formulario para enviar la solicitud-->

                            <div class="cursorMano" id="inscribirse" onclick="inscribirse(<?php echo $fila["idconcierto"]; ?>)">Inscribirse</div>

                        </td>
                        <?php
                        //si hay solicitudes en la tabla de propuestas
                    } else {
                        //en caso de que se haya hecho una solicitud
                        if ($existeSolicitud == true) {
                            if ($row["estadoPropuesta"] == 1) {
                                //aviso para el músico de que su solicitud está pendiente
                                if ($row["idconcierto"] == $fila["idconcierto"]) {
                                    ?>                                                                                    
                                    <td>

                                        <div id="desapuntarse" onclick="desapuntarse(<?php echo $fila["idconcierto"]; ?>)">Desapuntarse</div>

                                    </td>                                           
                                    <?php
                                }
                            }
                        }
                    }
                    echo '</tr>';
                }
            }
            ?>
        </table>
        <?php
        //en caso de que no haya conciertos de su género
    } else {
        echo "No hay conciertos de $datosGenero propuestos.";
    }
    ?>  
</div>

<div id="listadoConciertosAceptados">             
    <h1>Proximos conciertos: </h1>
    <?php
    //comprobamos que haya conciertos aceptados del genero del músico
    if (conciertosAceptadosPorGenero($datosGenero)) {
        //Recogemos todos los conciertos aceptados
        $conciertos = listadoConciertosAceptados();
        ?>
        <!--Creamos una tabla para el listado-->
        <table border = "1px black">
            <tr> <th> Nombre </th> <th> Fecha </th> <th> Hora </th> <th> Presupuesto </th> <th> Estado de la solicitud </th> </tr>
            <?php
            //pasamos los conciertos aceptados a un array asociativo
            while ($fila = mysqli_fetch_assoc($conciertos)) {
                //limitamos los conciertos a los que sean del mismo género del músico
                if (devolverGenero($fila["idgenero"]) == $datosGenero) {
                    echo '<tr>';
                    echo "<td class='datosBasicos'>" . $fila["nombre"] . '</td>';
                    echo "<td class='datosBasicos'>" . $fila["fecha"] . '</td>';
                    echo "<td class='datosBasicos'>" . $fila["hora"] . '</td>';
                    echo "<td class='datosBasicos'>" . $fila["presupuesto"] . '€' . '</td>';
                    if ($fila["idmusico"] == $idUsuario) {
                        echo "<td class='estadoSol' id='solAceptada'> Aceptada </td>";
                        // aviso para el músico si ha sido rechazado
                    } else if ($fila["idmusico"] != $idUsuario) {
                        echo "<td class='estadoSol' id='solRechazada'> Rechazada </td>";
                    }
                }
            }
            echo '</tr>';
            ?>
        </table>
        <?php
        //en caso de que no haya conciertos aceptados de su género
    } else {
        echo "No hay conciertos de $datosGenero aceptados.";
    }
    ?>  
</div>