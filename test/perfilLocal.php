<?php
require_once '../bbdd/bbdd.php';
session_start();
$username = $_SESSION["username"];
$idUsuario = recoger_ID_Usuario($username);
$datosUsus = datosUsuario($idUsuario);
$datosLocal = datosLocal($idUsuario);
$ciudad = tuCiudad($idUsuario);
$fotoActual = recogerFoto($idUsuario);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="../estilos/newPaginaPerfil.css" rel="stylesheet" type="text/css"/>
        <script src="../script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="../script/newPerfiles.js" type="text/javascript"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <title></title>
    </head>
    <body>
        <?php
        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];
            
            $foto = "icon-foto.png";
            
            $fila = datosUsuario($idUsuario);
            
            if ($fila["foto"] != ""){
                $foto = $fila["foto"];
                ?>
        <style>
            
            #fotoUsuario{
                border-radius: 100%;
                border: 2px solid white;
                height: 45px;
                width: 45px;
                top: 20px ;
            }
            
        </style>
        <?php
                
            }
            ?>
            <div id="container">
                <div id="barraSuperior">
                    <div id="logoImg">
                        <img src="../sources/Logo Valinor 2.png" alt=""/>
                        <p id="valinor">Valinor</p>
                        <p id="subValinor">Local</p>

                    </div><div id="restoBarra">
                        <p class='opciones' id="conciertos">Mis conciertos</p>
                        <p class='opciones' id="listaMusicosButton">Músicos</p>
                        <p class='opciones' id="listaLocalesButton">Listado de locales</p>
                        <p id="barrita">|</p>
                        <p id="username"><?php echo $username; ?></p>
                        <img id="fotoUsuario" src="<?php echo '../sources/'.$foto; ?>" alt=""/>
                        <!--src : ../sources/585e4beacb11b227491c3399.png-->
                    </div>
                </div>

                <div id='misConciertos'>


                    <div id="listado-conciertos-propuestos">
                        <h1>Mis conciertos</h1>
                        <?php
                        if (conciertosPropuestos()) {
                            $conciertos = listadoConciertosPropuestos();
                            ?>
                            <table>
                                <tr> <th style="border-radius: 5px 0px 0px 0px;"> Nombre </th> <th> Género </th> <th> Fecha </th> <th> Entrada </th> <th> Presupuesto </th> <th> Solicitudes </th> <th style="border-radius: 0px 5px 0px 0px;"> Cancelar </th> </tr>
                                <?php
                                echo '</tr>';
                            }
                        }
                        ?>
                    </table>
                </div>



                <script>

                    $(document).ready(function () {
                        mostrarConciertos();
                        cambiarFoto();

                    });


                    function cambiarFoto() {
                        let urlFoto = $("#foto").attr("src");
                        if (urlFoto === "../sources/") {
                            $("#foto").attr("src", "../sources/585e4beacb11b227491c3399.png");
                            
                        }else{
                            $("#foto").css({"border": "2px solid white"});
                            $("#foto").css({"border-radius": "28px"});
                            $("#foto").css({"height": "60%"});
                            $("#foto").css({"top": "19px"});

                            
                        }
                        
                    }

                    $("#solicitudes").click(function () {
                        $("#solicitudes").css({"background-color": "green"});
                    });

                    function aceptarMusico(idmusico, idconcierto) {
                        $.ajax({
                            url: "aceptarMusico.php",
                            type: "POST",
                            data: {"idconcierto": idconcierto, "idmusico": idmusico},
                            success: function (resp) {
                                $("#aceptar" + idmusico + idconcierto).html("Aceptado");

                                setTimeout(function () {
                                    mostrarConciertos();
                                    $("#containerSolicitantes").hide();
                                    $("#solicitudes").fadeOut();
                                }, 1000);

                            }
                        });
                    }

                    function mostrarSolicitudes(idconcierto) {
                        $.ajax({
                            context: this,
                            url: "musicosSolicitantes.php",
                            type: "POST",
                            data: {"idconcierto2": idconcierto},
                            success: function (resp) {
                                $('#solicitudes').html(resp);
                                $('#containerSolicitantes').fadeIn();
                                $('#solicitudes').fadeIn();

                            }

//                            success: (respJSON)=>{
//                                $(this).html(respJSON);
//                            }
                        });
                    }
                    ;



                    function crearConcierto() {
                        var nombre = $("#nombreConcierto").val();
                        var entrada = $("#entrada").val();
                        var presu = $("#presupuesto").val();
                        var fecha = $("#fecha").val();
                        var hora = $("#hora").val();
                        var genero = $("#genero").val();

                        $.ajax({
                            url: "crearConcierto.php",
                            type: "POST",
                            data: {"nombreConcierto": nombre, "entrada": entrada, "presupuesto": presu, "fecha": fecha, "hora": hora, "genero": genero},
                            success: function (resp) {
                                $('#resultadoAltaConcierto').html(resp);
                                mostrarConciertos();
                            }
                        });

                    }
                    function mostrarConciertos() {
                        $.ajax({
                            url: "conciertosLocal.php",
                            type: "POST",
                            success: function (resp) {
                                $('#listado-conciertos-propuestos').html(resp);
                            }
                        });
                    }
                    ;

                    function borrarConcierto(idconcierto) {
                        $.ajax({
                            url: "cancelarConcierto.php",
                            type: "POST",
                            data: {"idConcierto": idconcierto},
                            success: function (resp) {
                                mostrarConciertos();
                            }
                        });
                    }
                </script>


            </div>
            <div style="width: 100%; text-align: center">
                <div id="alta-concierto">

                    <h2>Nuevo concierto</h2>
                    <input type="text" id="nombreConcierto" minlength="1" maxlength="20" placeholder="Nombre del concierto" required>
                    <input type="number" id="entrada" min="0" required placeholder="Precio">
                    <input type="number" id="presupuesto" min="0" required placeholder="Presupuesto"><br>
                    <label>Fecha y hora: </label><br>
                    <input type="date" style="width: 42%" id="fecha" required>
                    <input type="time" style="width: 42%" id="hora" required><br>
                    <label>Género: </label><br>
                    <select id="genero" required>
                        <?php
                        $resultado = selectAllGeneros();
                        while ($fila = mysqli_fetch_assoc($resultado)) {
                            echo "<option value='" . $fila["idgenero"] . "'>" . $fila["nombre"] . "</option>";
                        }
                        ?>
                    </select><br><br>


                    <button class="cursorMano" id="crearConciertoButton" onclick="crearConcierto()">Crear concierto</button>
                    <div id="resultadoAltaConcierto"></div>

                </div>
            </div>

            <div id="listaLocales">
                <p id="locales">Locales</p>
                <?php
                $result = listaLocales();
                if (mysqli_num_rows($result) == 0) {
                    echo 'No hay ocales registrados';
                } else {


                    $row = mysqli_fetch_assoc($result);
                    echo '<table><tr>';
                    foreach ($row as $value => $key) {
                        echo '<th>' . $value . '</th>';
                    }
                    echo '</tr>';
                    do {
                        echo '<tr>';
                        foreach ($row as $value) {
                            echo '<td>' . $value . '</td>';
                        }
                        echo '</tr>';
                    } while ($row = mysqli_fetch_assoc($result));
                    echo '</table>';
                }
                ?>
            </div>
            <div id="listaMusicos">
                <p id="locales">Musicos</p>
                <table>
                    <tr> <th> Nombre </th> <th> Género </th> <th> E-mail </th> <th> Página web </th> <th> Teléfono </th> </tr>
                    <?php
                    $lista = listaDeMusicos();
                    while ($fila = mysqli_fetch_assoc($lista)) {
                        echo '<tr>';
                        echo '<td>' . $fila["nombre"] . '</td>';
                        echo '<td>' . $fila["genero"] . '</td>';
                        echo '<td>' . $fila["email"] . '</td>';
                        echo '<td>' . $fila["web"] . '</td>';
                        echo '<td>' . $fila["telefono"] . '</td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>

            <div id="containerSolicitantes">

            </div>
            <div id="solicitudes"></div>

        </div>
        <?php ?>
        <div id="usuario">
            <div id="triangulo"></div>
            <div id="opciones">
                <p><a href="../modificarFoto.php">Cambiar foto de perfil</a></p><br>
                <p><a href="../modificarDatos.php">Modificar datos</a></p><br>
                <p><a href="../modificarPasswrd.php">Cambiar contraseña</a></p><br>
                <p><a href="cerrarSesion.php">Salir</a></p><br>

            </div>
        </div>
    </body>
</html>
