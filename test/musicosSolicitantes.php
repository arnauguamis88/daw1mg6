<?php
session_start();
require_once '../bbdd/bbdd.php';

$idconcierto2 = $_POST["idconcierto2"];
$datosMusicoSolicitante = propuestasConcierto($idconcierto2);
?>

<div id="listadoPropuestas">
    <table border="1px black solid">
<!--        <tr> <th> Artista </th> <th> Contacto </th> <th> Propuesta </th> </tr>-->
        <?php
        while ($fila = mysqli_fetch_assoc($datosMusicoSolicitante)) {
            echo '<tr>';
            echo '<td>' . $fila["nombre"] . '</td>';
            echo '<td>' . $fila["telefono"] . '</td>';
            ?>
            <td>

                <input type="hidden" name="idmusico" value="<?php echo $fila["idusuario"] ?>">
                <input type="hidden" name="idconcierto2" value="<?php echo $idconcierto2 ?>">
                <div id="aceptar<?php echo $fila["idusuario"].$idconcierto2 ?>" onclick="aceptarMusico(<?php echo $fila["idusuario"] ?>,<?php echo $idconcierto2 ?>)">Aceptar</div>

            </td>
            <?php
            echo '</tr>';
        }

        if (isset($_POST["aceptarPropuesta"])) {
            $idmusico = $_POST["idmusico"];
            $idconcierto2 = $_POST["idconcierto2"];
            aceptarMusico($idconcierto2, $idmusico);
            echo 'Solicitud aprobada';
            sleep(1);

            header("Location: http://localhost/daw1mg6/test/perfilLocal.php");
        }
        ?>

    </table>
</div>