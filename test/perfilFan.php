<?php
require_once '../bbdd/bbdd.php';
session_start();
$username = $_SESSION["username"];
$idUsuario = recoger_ID_Usuario($username);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="../estilos/newPaginaPerfil.css" rel="stylesheet" type="text/css"/>
        <script src="../script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="../script/newPerfiles.js" type="text/javascript"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <title></title>
    </head>
    <body>
        <script>

            $(document).ready(function () {
                mostrarConciertosVotar();
                mostrarMusicosVotar();
                $("#punt1").css({"margin": "100px"});
            });

//            $("#votarConciertosButton").click(function(){
//                $("#listaMusicos").hide();
//                $("#listaLocales").hide();
//            });

            function mostrarMusicosVotar() {
                $.ajax({
                    url: "votarMusicos.php",
                    type: "POST",
                    success: function (resp) {
                        $('#votarMusicos').html(resp);
                    }
                });
            }
            ;

            function mostrarConciertosVotar() {
                $.ajax({
                    url: "votarConciertos.php",
                    type: "POST",
                    success: function (resp) {
                        $('#votarConciertos').html(resp);
                    }
                });
            }
            ;

            function likeMusico(idmusico) {
                let idusuario = <?php echo $idUsuario ?>;

                $.ajax({
                    url: "likeMusicos.php",
                    type: "POST",
                    data: {"idmusico": idmusico, "idfan": idusuario},
                    success: function () {
                        mostrarMusicosVotar();
                    }
                });

            }

            function likeConcierto(idconcierto) {
                let idusuario = <?php echo $idUsuario ?>;

                $.ajax({
                    url: "likeConcierto.php",
                    type: "POST",
                    data: {"idconcierto": idconcierto, "idusuario": idusuario},
                    success: function () {
                        mostrarConciertosVotar();
                    }
                });

            }





        </script>
        <style>
            #votarConciertos img{
                height: 15px;
                margin: 0;
                opacity: 60%;
            }

            #votarMusicos img{
                height: 15px;
                margin: 0;
                opacity: 60%;
            }

        </style>

        <?php
        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];

            $foto = "icon-foto.png";

            $fila = datosUsuario($idUsuario);

            if ($fila["foto"] != "") {
                $foto = $fila["foto"];
                ?>
                <style>

                    #fotoUsuario{
                        border-radius: 100%;
                        border: 2px solid white;
                        height: 45px;
                        width: 45px;
                        top: 20px ;
                    }

                </style>
                <?php
            }
            ?>
            <div id="container">
                <div id="barraSuperior">
                    <div id="logoImg">
                        <img src="../sources/Logo Valinor 2.png" alt=""/>
                        <p id="valinor">Valinor</p>
                        <p id="subValinor">Fan</p>
                    </div><div id="restoBarra">
                        <p class='opciones' id="votarConciertosButton">Votar</p>
                        <p class='opciones' id="listaMusicosButton">Músicos</p>
                        <p class='opciones' id="listaLocalesButton">Listado de locales</p>
                        <p id="barrita">|</p>
                        <p id="username"><?php echo $username; ?></p>
                        <img id="fotoUsuario" src="../sources/<?php echo $foto ?>" alt=""/>
                    </div>
                </div>

                <div id="votarConciertos">

                </div>

                <div id="votarMusicos">

                </div>

                <div id="listaLocales">
                    <p id="locales">Locales</p>
    <?php
    $result = listaLocales();
    if (mysqli_num_rows($result) == 0) {
        echo 'No hay ocales registrados';
    } else {


        $row = mysqli_fetch_assoc($result);
        echo '<table><tr>';
        foreach ($row as $value => $key) {
            echo '<th>' . $value . '</th>';
        }
        echo '</tr>';
        do {
            echo '<tr>';
            foreach ($row as $value) {
                echo '<td>' . $value . '</td>';
            }
            echo '</tr>';
        } while ($row = mysqli_fetch_assoc($result));
        echo '</table>';
    }
    ?>
                </div>
                <div id="listaMusicos">
                    <p id="locales">Musicos</p>
                    <table>
                        <tr> <th> Nombre </th> <th> Género </th> <th> E-mail </th> <th> Página web </th> <th> Teléfono </th> </tr>
    <?php
    $lista = listaDeMusicos();
    while ($fila = mysqli_fetch_assoc($lista)) {
        echo '<tr>';
        echo '<td>' . $fila["nombre"] . '</td>';
        echo '<td>' . $fila["genero"] . '</td>';
        echo '<td>' . $fila["email"] . '</td>';
        echo '<td>' . $fila["web"] . '</td>';
        echo '<td>' . $fila["telefono"] . '</td>';
        echo '</tr>';
    }
    ?>
                    </table>
                </div>
            </div>
    <?php
}
?>
        <div id="usuario">
            <div id="triangulo"></div>
            <div id="opciones">
                <p><a href="../modificarFoto.php">Cambiar foto de perfil</a></p><br>
                <p><a href="../modificarDatos.php">Modificar datos</a></p><br>
                <p><a href="../modificarPasswrd.php">Cambiar contraseña</a></p><br>
                <p><a href="cerrarSesion.php">Cerrar sesión</a></p><br>

            </div>
        </div>
    </body>
</html>
