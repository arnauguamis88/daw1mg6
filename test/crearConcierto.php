<?php
require_once '../bbdd/bbdd.php';
session_start();

$username = $_SESSION["username"];
$idlocal = recoger_ID_Usuario($username);
$idgenero = $_POST["genero"];
$nombreConcierto = $_POST["nombreConcierto"];
$fecha = $_POST["fecha"];
$hora = $_POST["hora"];
$presupuesto = $_POST["presupuesto"];
$entrada = $_POST["entrada"];

if ($idgenero == null || $nombreConcierto == null || $fecha == null || $hora == null || $presupuesto == null || $entrada == null) {
    echo 'Debes introducir todos los datos';
} else {
    if (conciertoDisponible($nombreConcierto)) {
        $result = crearConcierto($idlocal, $idgenero, $nombreConcierto, $fecha, $hora, $presupuesto, $entrada);
        echo 'Concierto registrado';
    } else {
        echo 'Nombre de concierto no disponible';
    }
}


