<?php
require_once '../bbdd/bbdd.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="../estilos/newPaginaPerfil.css" rel="stylesheet" type="text/css"/>
        <link href="../estilos/newHomepage.css" rel="stylesheet" type="text/css"/>
        <link href="../estilos/registro.css" rel="stylesheet" type="text/css"/>
        <link href="slick/slick.css" rel="stylesheet" type="text/css"/>
        <link href="slick/slick-theme.css" rel="stylesheet" type="text/css"/>
        <script src="../script/jquery-3.4.1.min.js" type="text/javascript"></script>

        <script src="slick/slick.min.js" type="text/javascript"></script>
        <script src="dist/jquery.validate.min.js" type="text/javascript"></script>
        <script src="dist/additional-methods.min.js" type="text/javascript"></script>
        <script src="../script/newPerfiles.js" type="text/javascript"></script>
        <script src="../script/registro.js" type="text/javascript"></script>
        <script src="../script/slider.js" type="text/javascript"></script> 
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <title>Página principal</title>
    </head>
    <body>
        <div id="fondo"></div>

        <div id="container">
            <div id="barraSuperior">
                <div id="logoImg">
                    <img src="../sources/Logo Valinor 2.png" alt=""/>
                    <p id="valinor">Valinor</p>
                </div><div id="restoBarra">
                    <p id="login">Log in</p>
                    <p id="signup">Sign up</p>
                </div>
            </div>

            <div id="sliderBox">
                <?php
                $datosConciertos = conciertoSlider();
                while ($row = mysqli_fetch_assoc($datosConciertos)) {
                    ?>
                    <div class="slider">
                        <div class="datosSlider">
                            <h1><?php echo $row["nombre"] ?></h1>
                            <p><?php echo $row["fecha"] ?></p>
                        </div>
                        <img src="../sources/<?php echo $row["foto"] ?>" alt=""/>
                    </div>
                    <?php
                }
                ?>
            </div>

            <div id="listaLocales">
                <h2>Lista de locales</h2>
                <?php
                $result = listaLocales();
                $row = mysqli_fetch_assoc($result);
                echo '<table><tr>';
                foreach ($row as $value => $key) {
                    echo '<th>' . $value . '</th>';
                }
                echo '</tr>';
                do {
                    echo '<tr>';
                    foreach ($row as $value) {
                        echo '<td>' . $value . '</td>';
                    }
                    echo '</tr>';
                } while ($row = mysqli_fetch_assoc($result));
                echo '</table>';
                ?>
            </div>

            <div id="listaMusicos">
                <?php
                $musicos = selectAllMusicos();
                if (mysqli_num_rows($musicos) == 0) {
                    echo "No hay músicos registrados. ";
                } else {
                    ?>
                    <table>
                        <tr> <th> Nombre </th> <th> Género musical </th> <th> E-mail </th> <th> Página web </th> <th> Teléfono de contacto </th> </tr>
                        <?php
                        $lista = listaDeMusicos();
                        while ($fila = mysqli_fetch_assoc($lista)) {
                            echo '<tr>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . $fila["genero"] . '</td>';
                            echo '<td>' . $fila["email"] . '</td>';
                            echo '<td>' . $fila["web"] . '</td>';
                            echo '<td>' . $fila["telefono"] . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>                       
                </table>
            </div>


            <div id="listaConciertos">
                <?php
                $conciertos = selectAllConciertos();
                if (mysqli_num_rows($conciertos) == 0) {
                    echo "No hay conciertos registrados. ";
                } else {
                    ?>
                    <table>
                        <h2> Lista de conciertos: </h2>
                        <tr> <th> Nombre </th> <th> Músico </th> <th> Género musical </th> <th> Fecha </th> <th> Hora </th> <th> Precio de entrada </th> </tr>
                        <?php
                        $lista = listaDeConciertos();
                        while ($fila = mysqli_fetch_assoc($lista)) {
                            echo '<tr>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . $fila["musico"] . '</td>';
                            echo '<td>' . $fila["genero"] . '</td>';
                            echo '<td>' . $fila["fecha"] . '</td>';
                            echo '<td>' . $fila["hora"] . '</td>';
                            echo '<td>' . $fila["entrada"] . "€" . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>                       
                </table>
            </div>


            <div class="topConciertos">
                <?php
                $conciertos = selectConciertosVotados();
                if (mysqli_num_rows($conciertos) == 0) {
                    echo "No hay conciertos puntuados. ";
                } else {
                    ?>
                    <table>
                        <h2> TOP 5 de conciertos: </h2>
                        <tr> <th> Ranking </th> <th> Nombre </th> <th> <img style="height: 15px;
                                                                                   margin-top: 3px;
                                                                                   opacity: 50%;
                                                                        ;" src="../sources/like-heart.png" alt=""/> </th> </tr>
                        <?php
                        $lista = topConciertos();
                        $contador = 1;
                        while ($fila = mysqli_fetch_assoc($lista)) {
                            echo '<tr>';
                            echo '<td>' . $contador . '</td>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . $fila["puntuacion"] . '</td>';
                            echo '</tr>';
                            $contador++;
                        }
                    }
                    ?>                       
                </table>
            </div>

            <div id="topMusicos">
                <?php
                $conciertos = selectMusicosVotados();
                if (mysqli_num_rows($conciertos) == 0) {
                    echo "No hay músicos puntuados. ";
                } else {
                    ?>
                    <table>
                        <h2> TOP 5 de músicos: </h2>
                        <tr> <th> Ranking </th> <th> Nombre </th> <th> <img style="height: 15px;
                                                                                   margin-top: 3px;
                                                                                   opacity: 50%;
                                                                        ;" src="../sources/like-heart.png" alt=""/> </th></tr>
                        
                        <?php
                        $lista = topMusicos();
                        $contador = 1;
                        while ($fila = mysqli_fetch_assoc($lista)) {
                            echo '<tr>';
                            echo '<td>' . $contador . '</td>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . $fila["puntuacion"] . '</td>';
                            echo '</tr>';
                            $contador++;
                        }
                    }
                    ?>                       
                </table>
            </div>

            <div id="containerLogin">


                <div id="formLogin">
                    <form method="POST" action="return false" onsubmit="return false">

                        <input type="text" name="user" id="user" value="" placeholder="Usuario"><br>
                        <input type="password" name="pass" id="pass" value="" placeholder="Password"><br>
                        <button onclick="validarLogin($('#user').val(), $('#pass').val());">Entrar</button>
                    </form>

                    <p id="resultado"></p>

                    <script>
                        function validarLogin(user, pass) {
                            $.ajax({
                                url: "validar.php",
                                type: "POST",
                                data: "user=" + user + "&pass=" + pass,
                                success: function (resp) {
                                    $('#resultado').html(resp);
                                }
                            });
                        }
                    </script>
                </div>

            </div>

            <div id="cont">
                <div id="global">
                    <div id="buttons">
                        <button id="buttonFan">Fan</button><button 
                            id="buttonLocal">Local</button><button 
                            id="buttonMusico">Musico</button>
                    </div>
                    <div id="fan">
                        <input type="text" id="nombreFan" name="nombre" placeholder="Nombre" required><br>
                        <input type="text" id="apellidoFan" name="apellido" placeholder="Apellidos" required><br>
                        <input type="text" id="nombreUsuFan" name="nombreUsuFan" placeholder="Nombre de usuario" required><br>
                        <input type="password" id="passwdFan" name="passwdFan" required
                               placeholder="Contraseña" maxlength="10"><br>
                        <input type="password" id="passwd2Fan" required
                               placeholder="Repita la contraseña" maxlength="10"><br>
                        <input type="email" id="emailFan" placeholder="Email" required><br>
                        <input type="tel" id="tlfFan" name="tlf" placeholder="Teléfono" required><br>
                        <p>Ciudad</p> <select name="ciudad" id="ciudadFan">
                            <?php
                            $ciudades = listarCiudades();
                            while ($row = mysqli_fetch_assoc($ciudades)) {
                                echo "<option value = '" . $row["idciudad"] . "'>";
                                echo $row["nombre"];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <p>Fecha de nacimiento</p> <input type="date" name="bday" id="bdayFan" required
                                                          style="margin-top: 0"><br>
                        <button class="enviar" id="validarFan">Registrarse</button>

                    </div>

                    <div id="local" >
                        <input type="text" id="nombreLocal"name="nombre" placeholder="Nombre del local" required><br>
                        <input type="text" id="nombreUsuLocal" placeholder="Nombre de usuario" required><br>
                        <input type="password" id="passwdLocal" required
                               placeholder="Contraseña" max="10"><br>
                        <input type="password" id="passwd2Local" required
                               placeholder="Repita la contraseña" max="10"><br>
                        <input type="email" id="emailLocal" placeholder="Email" required><br>
                        <input type="tel" name="tlf" id="tlfLocal" placeholder="Teléfono" required><br>
                        <input type="number" name="aforo" id="aforo" min="0" placeholder="Aforo máximo" required><br>
                        <input type="text" id="direccion" name="direccion" placeholder="Direccion">
                        <p>Ciudad</p> <select name="ciudad" id="ciudadLocal">
                            <?php
                            $ciudades = listarCiudades();
                            while ($row = mysqli_fetch_assoc($ciudades)) {
                                echo "<option value = '" . $row["idciudad"] . "'>";
                                echo $row["nombre"];
                                echo "</option>";
                            }
                            ?>
                        </select>                        
                        <button class="enviar" onclick="validarLocal();">Registrarse</button>

                    </div>

                    <div id="musico">
                        <input type="text" name="nombre" id="nombreMusico" placeholder="Nombre" required><br>
                        <input type="text" id="nombreUsuMusico" placeholder="Nombre de usuario" required><br>
                        <input type="password" id="passwdMusico" required
                               placeholder="Contraseña" max="10"><br>
                        <input type="password" id="passwd2Musico" required
                               placeholder="Repita la contraseña" max="10"><br>
                        <input type="email" id="emailMusico" placeholder="Email" required><br>
                        <input type="tel" name="tlf" id="tlfMusico" placeholder="Teléfono" required><br>
                        <input type="number" min="1" name="componentes" id="componentes" placeholder="Número de componentes"required>
                        <input type="text" placeholder="Web" id="web" name="web">
                        <p>Ciudad</p> <select name="ciudad" id="ciudadMusico">
                            <?php
                            $ciudades = listarCiudades();
                            while ($row = mysqli_fetch_assoc($ciudades)) {
                                echo "<option value = '" . $row["idciudad"] . "'>";
                                echo $row["nombre"];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <p>Genero</p> <select name="genero" id="generoMusico" required>
                            <?php
                            $resultado = selectAllGeneros();
                            while ($fila = mysqli_fetch_assoc($resultado)) {
                                echo "<option value='" . $fila["idgenero"] . "'>" . $fila["nombre"] . "</option>";
                            }
                            ?>
                        </select>
                        <button class="enviar" onclick="validarMusico();">Registrarse</button>

                    </div>


                    <div id="resultadoRegistro"></div>
                    




                    <script>

                        $("#validarFan").click(function () {
                            var user = $("#nombreUsuFan").val();
                            var nombre = $("#nombreFan").val();
                            var apellido = $("#apellidoFan").val();
                            var pass = $("#passwdFan").val();
                            var pass2 = $("#passwd2Fan").val();
                            var mail = $("#emailFan").val();
                            var tlf = $("#tlfFan").val();
                            var bday = $("#bdayFan").val();
                            var ciudad = $("#ciudadMusico").val();

                            $.ajax({
                                url: "validarRegistro.php",
                                type: "POST",
                                data: "user=" + user + "&pass=" + pass + "&pass2=" + pass2 + "&mail=" + mail + '&nombre=' + nombre
                                        + '&ciudad=' + ciudad + '&apellido=' + apellido + '&tlf=' + tlf + '&bday=' + bday,
                                success: function (resp) {
                                    $('#resultadoRegistro').html(resp);
                                }
                            });
                        });

                        function validarLocal() {
                            var user = $("#nombreUsuLocal").val();
                            var pass = $("#passwdLocal").val();
                            var pass2 = $("#passwd2Local").val();
                            var mail = $("#emailLocal").val();
                            var aforo = $("#aforo").val();
                            var nombre = $("#nombreLocal").val();
                            var tlf = $("#tlfLocal").val();
                            var ciudad = $("#ciudadMusico").val();
                            var direccion = $("#direccion").val();

                            $.ajax({
                                url: "validarRegistroLocal.php",
                                type: "POST",
                                data: "user=" + user + "&pass=" + pass + "&pass2=" + pass2 + "&mail=" + mail + "&ciudad=" + ciudad
                                        + "&aforo=" + aforo + "&nombre=" + nombre + "&tlf=" + tlf + "&direccion=" + direccion,
                                success: function (resp) {
                                    $('#resultadoRegistro').html(resp);
                                }
                            });
                        }

                        function validarMusico() {
                            var user = $("#nombreUsuMusico").val();
                            var pass = $("#passwdMusico").val();
                            var pass2 = $("#passwd2Musico").val();
                            var mail = $("#emailMusico").val();
                            var componentes = $("#componentes").val();
                            var nombre = $("#nombreMusico").val();
                            var tlf = $("#tlfMusico").val();
                            var web = $("#web").val();
                            var ciudad = $("#ciudadMusico").val();
                            var genero = $("#generoMusico").val();
                            $.ajax({
                                url: "validarRegistroMusico.php",
                                type: "POST",
                                data: "user=" + user + "&pass=" + pass + "&pass2=" + pass2 + "&mail=" + mail +
                                        "&componentes=" + componentes + "&nombre=" + nombre + "&tlf=" + tlf + "&web=" + web
                                        + "&ciudad=" + ciudad + "&genero=" + genero,
                                success: function (resp) {
                                    $('#resultadoRegistro').html(resp);
                                }
                            });
                        }
                    </script>
                </div>

            </div>

        </div>

    </body>
</html>
