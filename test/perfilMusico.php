<?php
require_once '../bbdd/bbdd.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="../estilos/newPaginaPerfil.css" rel="stylesheet" type="text/css"/>
        <script src="../script/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="../script/newPerfiles.js" type="text/javascript"></script>
        <script src="../script/perfil_musico_acciones.js" type="text/javascript"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <title></title>
    </head>

    <script>

//        $("#valinor").click(mostrarPropuestas());

        $("#propuestasMusico").click(function () {
            $("#listaLocalesButton").hide();
            $("#listaMusicosButton").hide();
            $("#altaBajaConciertos").show();
        });

        $("#listaLocalesButton").click(function () {
            $("#listaLocales").show();
            $("#listaMusicos").hide();
            $("#altaBajaConciertos").hide();
        });

        $("#listaMusicosButton").click(function () {
            $("#listaMusicosButton").show();
            $("#listaLocalesButton").hide();
            $("#altaBajaConciertos").hide();
        });
        
        function inscribirse (idconcierto){
            $.ajax({
                url: "apuntarse.php",
                type: "POST",
                data: {"idconcierto":idconcierto},
                success: function (resp) {
                    mostrarPropuestas();
                }
            });
        }
        function desapuntarse (idconcierto){
            $.ajax({
                url: "desapuntarse.php",
                type: "POST",
                data: {"idconcierto":idconcierto},
                success: function (resp) {
                    mostrarPropuestas();
                }
            });
        }

        function mostrarPropuestas() {
            $.ajax({
                url: "conciertosPropuestosMusico.php",
                type: "POST",
                success: function (resp) {
                    $('#conciertosPropuestos').html(resp);
                }
            });
        }

        $(document).ready(function () {
            mostrarPropuestas();
        });


    </script>

    <body>
        <?php
        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];
            $idUsuario = recoger_ID_Usuario($username);
            $datosGenero = generoMusico($idUsuario);
            
            $foto = "icon-foto.png";
            
            $fila = datosUsuario($idUsuario);
            
            if ($fila["foto"] != ""){
                $foto = $fila["foto"];
                ?>
        <style>
            
            #fotoUsuario{
                border-radius: 100%;
                border: 2px solid white;
                height: 45px;
                width: 45px;
                top: 20px ;
            }
            
        </style>
        <?php
                
            }
            ?>
            <div id="container">
                <div id="barraSuperior">
                    <div id="logoImg">
                        <img src="../sources/Logo Valinor 2.png" alt=""/>
                        <p id="valinor">Valinor</p>
                        <p id="subValinor">Músico</p>
                    </div><div id="restoBarra">
                        <p class='opciones' id="propuestasMusico">Mis conciertos</p>
                        <p class='opciones' id="listaMusicosButton">Músicos</p>
                        <p class='opciones' id="listaLocalesButton">Listado de locales</p>
                        <p id="barrita">|</p>
                        <p id="username"><?php echo $username; ?></p>
                        <img id="fotoUsuario" src="../sources/<?php echo $foto; ?>" alt=""/>
                    </div>
                </div>

                <div id="conciertosPropuestos">
                    
                </div>
                

                <div id="listaLocales">
                    <p id="locales">Locales</p>
                    <?php
                    $result = listaLocales();
                    if (mysqli_num_rows($result) == 0) {
                        echo 'No hay ocales registrados';
                    } else {


                        $row = mysqli_fetch_assoc($result);
                        echo '<table><tr>';
                        foreach ($row as $value => $key) {
                            echo '<th>' . $value . '</th>';
                        }
                        echo '</tr>';
                        do {
                            echo '<tr>';
                            foreach ($row as $value) {
                                echo '<td>' . $value . '</td>';
                            }
                            echo '</tr>';
                        } while ($row = mysqli_fetch_assoc($result));
                        echo '</table>';
                    }
                    ?>
                </div>
                <div id="listaMusicos">
                    <p id="locales">Musicos</p>
                    <table>
                        <tr> <th> Nombre </th> <th> Género </th> <th> E-mail </th> <th> Página web </th> <th> Teléfono </th> </tr>
                        <?php
                        $lista = listaDeMusicos();
                        while ($fila = mysqli_fetch_assoc($lista)) {
                            echo '<tr>';
                            echo '<td>' . $fila["nombre"] . '</td>';
                            echo '<td>' . $fila["genero"] . '</td>';
                            echo '<td>' . $fila["email"] . '</td>';
                            echo '<td>' . $fila["web"] . '</td>';
                            echo '<td>' . $fila["telefono"] . '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </table>
                </div>
            </div>
            <?php
        }

        if (isset($_POST["desapuntarse"])) {
            $idconcierto = $_POST["idconcierto"];
            $nombreConcierto = $_POST["nombreconcierto"];
            $resultado = desapuntarMusico($idUsuario, $idconcierto);
            echo "<div class='respuesta'> Solicitud cancelada para: $nombreConcierto. </div>";
        }

        if (isset($_POST["apuntarse"])) {
            $idconcierto = $_POST["idconcierto"];
            $nombreConcierto = $_POST["nombreconcierto"];
            $resultado = apuntarMusico($idUsuario, $idconcierto);
            echo "<div class='respuesta'> Solicitud enviada para: $nombreConcierto. </div>";
        }
        ?>
        <div id="usuario">
            <div id="triangulo"></div>
            <div id="opciones">
                <p><a href="../modificarFoto.php">Cambiar foto de perfil</a></p><br>
                <p><a href="../modificarDatos.php">Modificar datos</a></p><br>
                <p><a href="../modificarPasswrd.php">Cambiar contraseña</a></p><br>
                <p><a href="cerrarSesion.php">Salir</a></p><br>

            </div>
        </div>
    </body>
</html>
