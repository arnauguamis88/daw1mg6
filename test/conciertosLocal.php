<?php
require_once '../bbdd/bbdd.php';
session_start();
?>

<h1>Mis conciertos</h1>
<?php
if (conciertosPropuestos()) {
    $conciertos = listadoConciertosPropuestos();
    ?>
    <table border="1px black solid">
        <tr> <th> Nombre </th> <th> Género </th> <th> Fecha </th> <th> Entrada </th> <th> Presupuesto </th> <th> Solicitudes </th> <th> Cancelar </th> </tr>
        <?php
        $cont = 0;
        while ($fila = mysqli_fetch_assoc($conciertos)) {
            $cont++;
            echo '<tr>';
            echo '<td>' . $fila["nombre"] . '</td>';
            echo '<td>' . devolverGenero($fila["idgenero"]) . '</td>';
            echo '<td>' . $fila["fecha"] . '<br>';
            echo $fila["hora"];
            echo '<td>' . $fila["entrada"] . '€' . '</td>';
            echo '<td>' . $fila["presupuesto"] . '€' . '</td>';
            ?>
            <td>
                <div id="solicitudes<?php echo$cont;?>"></div> 
                <div name="verPropuestas" class="cursorMano" onclick="mostrarSolicitudes(<?php echo $fila["idconcierto"];?>)"><?php
                    echo propuestasTotales($fila["idconcierto"]);
                    if (propuestasTotales($fila["idconcierto"]) > 0) {
                        echo ' - Ver';
                    }
                    ?></div>

            </td>
            <?php ?>
            <td>



                <input type = "hidden" name = "idconcierto" value = " ">
        <!--                <input type = "submit" class="cancelar" name = "cancelar" value = "">-->
                <div class="cursorMano" onclick="borrarConcierto('<?php echo $fila["idconcierto"] ?>')">X</div>
            </td>

            <script>



            </script>

            <?php
        }
        if (isset($_POST["cancelar"])) {
            $idconcierto = $_POST["idConcierto"];
            $cancelar = cancelarConcierto($idconcierto);

            if ($cancelar == true) {
                echo "Concierto cancelado";
            } else {
                echo $cancelar;
            }
        }
        echo '</tr>';
    } else {
        echo "No hay ningún concierto propuesto";
    }
    ?>
</table>