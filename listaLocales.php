<?php
require_once 'bbdd/bbdd.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <style>
        td,th{
            text-align: center;
            border-collapse: collapse;
            width: 90px;
            border-top: 1px black solid;
            border-bottom: 1px black solid;
        }
        table{
            border-collapse: collapse;
            font-family: serif;
            border: 1px black solid;
            font-family: monospace;
        }
        tr:nth-child(2n){
            background-color: #7bb2b0;
        }
        tr:nth-child(2n-1){
            background-color: #a0c5bd;
        }
    </style>
    <body>
        <?php
        $result = listaLocales();
        $row = mysqli_fetch_assoc($result);
        echo '<table><tr>';
        foreach ($row as $value => $key) {
            echo '<th>' . $value . '</th>';
        }
        echo '</tr>';
        do {
            echo '<tr>';
            foreach ($row as $value) {
                echo '<td>' . $value . '</td>';
            }
            echo '</tr>';
        } while ($row = mysqli_fetch_assoc($result));
        echo '</table>';
        ?>

    </body>
</html>
