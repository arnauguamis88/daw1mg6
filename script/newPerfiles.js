/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    
    
    
    $('#containerSolicitantes').click(function(){
        $('#containerSolicitantes').fadeOut();
        $('#solicitudes').fadeOut();
    });
    
    $("#propuestasMusico").click(function(){
        $("#conciertosPropuestos").show();
        $("#alta-concierto").show();
        $("#listaLocales").hide();
        $("#listaMusicos").hide();
    });
    
    $("#conciertos").click(function(){
        $("#misConciertos").show();
        $("#listaLocales").hide();
        $("#listaMusicos").hide();
        $("#alta-concierto").show();

    });

    $("#listaLocalesButton").click(function () {
        $("#listaLocales").show();
        $("#listaMusicos").hide();
        $("#misConciertos").hide();
        $("#conciertosPropuestos").hide();
        $("#alta-concierto").hide();
        $("#votarConciertos").hide();
        $("#votarMusicos").hide();
    });

    $("#listaMusicosButton").click(function () {
        $("#listaMusicos").show();
        $("#listaLocales").hide();
        $("#misConciertos").hide();
        $("#conciertosPropuestos").hide();
        $("#alta-concierto").hide();
        $("#votarConciertos").hide();
        $("#votarMusicos").hide();

    });
    
    $("#votarConciertosButton").click(function () {
        $("#votarConciertos").show();
        $("#votarMusicos").show();
        $("#listaMusicos").hide();
        $("#listaLocales").hide();
    });
    
    $("#restoBarra>img").click(function () {
        $("#usuario").toggle();
    });

    $("#formLogin").hide();
    $("#cont").hide();
    $("#fondo").hide();
    $("#login").click(function () {
        $("#formLogin").fadeIn();
        $("#fondo").fadeIn();
    });
    $("#fondo").click(function () {
        $("#formLogin").fadeOut();
        $("#fondo").fadeOut();
        $("#cont").fadeOut();
    });


    $("#signup").click(function () {
        $("#cont").fadeIn();
        $("#fondo").fadeIn();
    });
});



