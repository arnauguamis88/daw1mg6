
$(document).ready(function () {

    $("header").hide();
    $("#main").hide();
    $(".easterEgg").hide();

    $("#logoIntro").click(function () {
        $("#logoIntro").hide();
        $("header").fadeIn(1200);
        $("#main").fadeIn(1200);
    });

    $("#logoImg").click(function () {
        $(".easterEgg").css({"margin-top": "55px"});
        $("header").hide();
        $("#main").hide();
        $(".easterEgg").fadeIn(1200);
    });

    $("#leido").click(function () {
        $(".easterEgg").css({"margin-top": "0px"});
        $(".easterEgg").hide();
        $("header").fadeIn(1200);
        $("#main").fadeIn(1200);
    });

});