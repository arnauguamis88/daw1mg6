
$(document).ready(function () {
    /*Mantenemos el menu y el tablero de modificaciones oculto*/
    quitarMenu();
    $("#main").hide();

    //al hacer click muestra o esconde el menu según en que estado estuviese
    $("#botonMenu").click(moverMenu);
    //al hacer click muestra o esconde el tablero de modificaciones 
    //según en que estado estuviese
    $("#fotoPerfil").click(function () {
        $("#usuario").toggle();
    });

//Al hacer click cambia la estructura y color del botón del menu
    $('#botonMenu').click(function () {
        $('#botonMenu').toggleClass('open');
    });

//Al clickar sobre "Mis datos" muestra la tabla de datos
    $("#misDatos").click(function () {
        if (mostrarDatos) {
            $("#main").hide();
            mostrarDatos = false;
        } else {
            $("#main").show();
            if ($("#alta-concierto").show()) {
                $("#alta-concierto").hide()
                altaVisible = false;
            }
            if ($("#baja-concierto").show()) {
                $("#baja-concierto").hide()
                bajaVisible = false;
            }
            if ($("#listado-conciertos-propuestos").show()) {
                $("#listado-conciertos-propuestos").hide()
                listadoVisible = false;
            }
            if ($("#listadoPropuestas").show()) {
                $("#listadoPropuestas").hide()
            }
            mostrarDatos = true;
        }

    })

//el menu se cierra si se selecciona alguna opción
    $(".apartado").click(function () {
        $('#botonMenu').toggleClass('open');
        if (mostrandoMenu) {
            quitarMenu();
            mostrandoMenu = false;
        } else {
            mostrarMenu();
            mostrandoMenu = true;
        }
    })

});

//variable global para las funciones
let mostrandoMenu = false;
let mostrarDatos = false;

//función para mover el menu
function moverMenu() {
    if (mostrandoMenu) {
        quitarMenu();
        mostrandoMenu = false;
    } else {
        mostrarMenu();
        mostrandoMenu = true;
    }
}

//funciones para modificar el botón
function quitarMenu() {
    $("#menu").animate({"left": "-550px"});
    $(".spanes").css("background-color", "white");
}

function mostrarMenu() {
    $("#menu").animate({"left": "0px"});
    $(".spanes").css("background-color", "red");
}
