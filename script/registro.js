/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $("#local").hide();
    $("#musico").hide();
    $("#buttonFan").click(verFan);
    $("#buttonLocal").click(verLocal);
    $("#buttonMusico").click(verMusico);
    $("#buttonFan").css({"background-color":"#005156"});
    $("#buttonLocal").css({"background-color":"#003135"});
    $("#buttonMusico").css({"background-color":"#003135"});
    $("#buttonFan").css({"text-shadow":"-2px 1.5px 1px black"});


    function verFan() {
        $("#local").hide();
        $("#musico").hide();
        $("#fan").fadeIn().animate({duration: 0});
        $("#buttonFan").css({"background-color":"#005156"});
        $("#buttonFan").css({"text-shadow":"-2px 1.5px 1px black"});
        $("#buttonLocal").css({"text-shadow":"none"});
        $("#buttonMusico").css({"text-shadow":"none"});

        $("#buttonLocal").css({"background-color":"#003135"});
        $("#buttonMusico").css({"background-color":"#003135"});
        
        //text-shadow: -2px 1.5px 1px black;
    }
    
    function verLocal() {
        $("#local").fadeIn().animate({duration: 0});
        $("#fan").hide();
        $("#musico").hide();
        $("#buttonLocal").css({"background-color":"#005156"});
        $("#buttonFan").css({"background-color":"#003135"});
        $("#buttonMusico").css({"background-color":"#003135"});
        $("#buttonLocal").css({"text-shadow":"-2px 1.5px 1px black"});
        $("#buttonFan").css({"text-shadow":"none"});
        $("#buttonMusico").css({"text-shadow":"none"});
    }
    
    function verMusico() {
        $("#musico").fadeIn().animate({duration: 0});
        $("#fan").hide();
        $("#local").hide();
        $("#buttonMusico").css({"background-color":"#005156"});
        $("#buttonFan").css({"background-color":"#003135"});
        $("#buttonLocal").css({"background-color":"#003135"});
        $("#buttonMusico").css({"text-shadow":"-2px 1.5px 1px black"});
        $("#buttonLocal").css({"text-shadow":"none"});
        $("#buttonFan").css({"text-shadow":"none"});
    }
    
    
    
    
    
});
