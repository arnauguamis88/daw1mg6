$(document).ready(function () {

    //escondemos alta, baja y listado
    $("#alta-concierto").hide();
    $("#baja-concierto").hide();
    $("#listado-conciertos-propuestos").hide();

//muestra y oculta el alta de conciertos al apretar su opción en el menu 
    $("#alta").click(function () {
        $("#alta-concierto").show();
        if (altaVisible) {
            $("#alta-concierto").hide();
            altaVisible = false;
        } else {
            $("#alta-concierto").show().css("margin-left", "39%");
            if ($("#baja-concierto").show()) {
                $("#baja-concierto").hide()
                bajaVisible = false;
            }
            if ($("#listado-conciertos-propuestos").show()) {
                $("#listado-conciertos-propuestos").hide()
                listadoVisible = false;
            }
            if ($("#main").show()) {
                $("#main").hide()
                mostrarDatos = false;
            }
            if ($("#listadoPropuestas").show()) {
                $("#listadoPropuestas").hide()
            }
            altaVisible = true;
        }
    })

//muestra y oculta la baja de conciertos al apretar su opción en el menu 
    $("#baja").click(function () {
        $("#baja-concierto").show();
        if (bajaVisible) {
            $("#baja-concierto").hide();
            bajaVisible = false;
        } else {
            $("#baja-concierto").show().css("margin-left", "36.5%");
            if ($("#alta-concierto").show()) {
                $("#alta-concierto").hide()
                altaVisible = false;
            }
            if ($("#listado-conciertos-propuestos").show()) {
                $("#listado-conciertos-propuestos").hide()
                listadoVisible = false;
            }
            if ($("#main").show()) {
                $("#main").hide()
                mostrarDatos = false;
            }
            if ($("#listadoPropuestas").show()) {
                $("#listadoPropuestas").hide()
            }
            bajaVisible = true;
        }
    })

//muestra y oculta el listado de conciertos propuestos al apretar su opción en el menu 
    $("#listado").click(function () {
        $("#listado-conciertos-propuestos").show();
        if (listadoVisible) {
            $("#listado-conciertos-propuestos").hide();
            listadoVisible = false;
        } else {
            $("#listado-conciertos-propuestos").show().css("margin-left", "32.3%");
            if ($("#baja-concierto").show()) {
                $("#baja-concierto").hide()
                bajaVisible = false;
            }
            if ($("#alta-concierto").show()) {
                $("#alta-concierto").hide()
                altaVisible = false;
            }
            if ($("#main").show()) {
                $("#main").hide()
                mostrarDatos = false;
            }
            if ($("#listadoPropuestas").show()) {
                $("#listadoPropuestas").hide()
            }
            listadoVisible = true;
        }
    })

});

let altaVisible = false;
let bajaVisible = false;
let listadoVisible = false;