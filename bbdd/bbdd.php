<?php

////////////////////////////////////////////////////////////////////////////////
//                            CONECTAR Y DESCONECTAR
////////////////////////////////////////////////////////////////////////////////

function conectar() {
    $c = mysqli_connect("localhost", "root", "", "daw1mg6");
    if (!$c) {
        die("Error en la conexión");
    }
    return $c;
}

function desconectar($c) {
    mysqli_close($c);
}

function query($query) {
    $c = conectar();
    $resultado = mysqli_query($c, $query);
    desconectar($c);
    return $resultado;
}

////////////////////////////////////////////////////////////////////////////////
//                            
////////////////////////////////////////////////////////////////////////////////

function verificarDatos($username, $passwrd) {
    $c = conectar();
    $select = "select * from usuario where username = '$username'";
    $comprobar = mysqli_query($c, $select);
    if (mysqli_num_rows($comprobar) == 1) {
        $hash = mysqli_fetch_assoc($comprobar);
        $resultado = password_verify($passwrd, $hash["passwrd"]);
    } else {
        $resultado = false;
    }
    desconectar($c);
    return $resultado;
}

function recogerTipo($username) {
    $c = conectar();
    $select = "select tipo from usuario where username = '$username'";
    $resultado = mysqli_query($c, $select);
    if (!$resultado) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_row($resultado);
    }
    desconectar($c);
    return $fila[0];
}

function devolverGenero($idgenero) {
    $c = conectar();
    $select = "select nombre from genero where idgenero = '$idgenero'";
    $resultado = mysqli_query($c, $select);
    if (!$resultado) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_row($resultado);
    }
    desconectar($c);
    return $fila[0];
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
//da el nombre de la ciudad del usuario
function tuCiudad($idUsuario) {
    $c = conectar();
    $select = "select ciudad.nombre from ciudad inner join usuario on "
            . "ciudad.idciudad=usuario.idciudad where usuario.idusuario = '$idUsuario'";
    $resultado = mysqli_query($c, $select);
    if ($resultado === false) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_assoc($resultado);
    }
    desconectar($c);
    return $fila["nombre"];
}

//da el nombre del género del músico
function generoMusico($idUsuario) {
    $c = conectar();
    $select = "select genero.nombre from genero inner join musico on "
            . "genero.idgenero=musico.idgenero where musico.idmusico = '$idUsuario'";
    $resultado = mysqli_query($c, $select);
    if ($resultado === false) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_assoc($resultado);
    }
    desconectar($c);
    return $fila["nombre"];
}

//------------------------------------------------------------------------------
//                                  REGISTRO
//------------------------------------------------------------------------------

function conciertoDisponible($nombre) {
    $select = "select * from concierto where nombre = '$nombre'";
    if (mysqli_num_rows(query($select)) == 0) {
        return true;
    } else {
        return false;
    }
}

function crearConcierto($idlocal, $idgenero, $nombreConcierto, $fecha, $hora, $presupuesto, $entrada) {
    $c = conectar();
    $insert = "INSERT INTO daw1mg6.concierto (idlocal, idgenero, nombre, fecha, hora, presupuesto, entrada, estadoConcierto)"
            . "VALUES ($idlocal, $idgenero, '$nombreConcierto', '$fecha', '$hora', $presupuesto, $entrada, 1);";
    if (mysqli_query($c, $insert)) {
        $resultado = true;
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function cancelarConcierto($idconcierto) {
    $c = conectar();
    $delete = "update concierto set estadoConcierto = 3 where idconcierto = $idconcierto";
    $result = mysqli_query($c, $delete);
    desconectar($c);
    return $result;
}

function conciertosPropuestos() {
    $select = "select * from concierto where estadoConcierto = 1";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        return true;
    }
}

function propuestasTotales($idconcierto) {
    $c = conectar();
    $select = "select count(idconcierto) from propuesta where idconcierto = $idconcierto AND estadoPropuesta = 1";
    $resultado = mysqli_query($c, $select);
    if (!$resultado) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_row($resultado);
    }
    desconectar($c);
    return $fila[0];
}

function propuestasConcierto($idconcierto) {
    $c = conectar();
    $select = "select usuario.idusuario, usuario.nombre, usuario.telefono from usuario inner join propuesta 
               on usuario.idusuario = propuesta.idmusico
               where propuesta.idconcierto = $idconcierto AND propuesta.estadoPropuesta = 1;";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function listadoConciertosPropuestos() {
    $c = conectar();
    $select = "select * from concierto where estadoConcierto = 1";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function aceptarMusico($idconcierto, $idmusico) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c);
        $modificar1 = "update concierto set idmusico = $idmusico, estadoConcierto = 2 where idconcierto = $idconcierto";
        $resultado1 = mysqli_query($c, $modificar1);
        $modificar2 = "update propuesta set estadoPropuesta = 2 where idconcierto = $idconcierto AND idmusico = $idmusico";
        $resultado2 = mysqli_query($c, $modificar2);
        if ($resultado1 && $resultado2) {
            echo "Músico Aceptado<br>";
        }
        mysqli_commit($c);
    } catch (Exception $ex) {
        mysqli_rollback($c);
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

/* * *********Listado de conciertos del género del músico********** */

//Para saber si hay conciertos propuestos del género del músico
function conciertosPropuestosPorGenero($generoMusico) {
    $select = "select * from concierto
               inner join genero on concierto.idgenero=genero.idgenero
               where estadoConcierto = 1 and genero.nombre='$generoMusico';";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        return true;
    }
}

//Para saber si hay conciertos aceptados del género del músico
function conciertosAceptadosPorGenero($generoMusico) {
    $select = "select * from concierto
               inner join genero on concierto.idgenero=genero.idgenero
               where estadoConcierto = 2 and genero.nombre='$generoMusico';";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        return true;
    }
}

//recoge todos los conciertos aceptados del género del músico
function listadoConciertosAceptados() {
    $c = conectar();
    $select = "select * from concierto where estadoConcierto = 2";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

//Para saber si hay propuestas 
function existenPropuestas($idmusico) {
    $select = "select * from propuesta where idmusico = '$idmusico'";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        return true;
    }
}

//Para saber si hay propuestas 
function selectMusicoConciertoAceptado($idconcierto) {
    $c = conectar();
    $select = "select * from concierto where estadoConcierto = 2 and idconcierto = '$idconcierto'";
    $query = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($query);
    desconectar($c);
    return $fila;
}

//Para saber si el músico se ha apuntado o no a un concierto propuesto
function existeSolicitud($idmusico, $idconcierto) {
    $select = "select * from propuesta where idmusico = '$idmusico' and idconcierto = '$idconcierto'";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        return true;
    }
}

//Para obtener los datos de una propuesta según el músico y concierto
function estadoPropuestas($idmusico, $idconcierto) {
    $c = conectar();
    $select = "select * from propuesta where idmusico ='$idmusico' and idconcierto = '$idconcierto'";
    $query = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($query);
    desconectar($c);
    return $fila;
}

//Inserta una solicitud concreta del músico en la tabla propuesta
function apuntarMusico($idmusico, $idconcierto) {
    $c = conectar();
    $update = "INSERT INTO propuesta (idmusico, idconcierto, estadoPropuesta)"
            . "VALUES ('$idmusico', '$idconcierto', '1');";
    if (mysqli_query($c, $update) === false) {
        $resultado = mysqli_error($c);
    } else {
        $resultado = "ok";
    }
    desconectar($c);
    return $resultado;
}

//Elimina una solicitud concreta del músico
function desapuntarMusico($idmusico, $idconcierto) {
    $c = conectar();
    $update = "delete from propuesta where idmusico = '$idmusico' and idconcierto = '$idconcierto';";
    if (mysqli_query($c, $update) === false) {
        $resultado = mysqli_error($c);
    } else {
        $resultado = "ok";
    }
    desconectar($c);
    return $resultado;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function existeUsername($username) {
    $select = "select * from usuario where username = '$username'";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        return true;
    }
}

function selectIdByUsername($username) {
    $select = "select idusuario from usuario where username = '$username'";
    $result = query($select);
    if (mysqli_num_rows($result) == 0) {
        return false;
    } else {
        $fila = mysqli_fetch_assoc($result);
        return $fila["idusuario"];
    }
}

function registroFan($username, $passwd, $nombre, $apellido, $email, $tlf, $idciudad, $fechaNacimiento) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c, MYSQLI_TRANS_START_READ_WRITE);
        $query = "insert into usuario values (null, '$username', '$passwd', '$nombre', '$email', '', '$tlf', $idciudad, 1)";
        $resultado = mysqli_query($c, $query);
        if ($resultado === false) {
            echo mysqli_error($c);
        }
        $id = mysqli_insert_id($c);
        $query2 = "insert into fan values ($id, '$apellido', now(), '$fechaNacimiento')";
        $resultado2 = mysqli_query($c, $query2);
        if ($resultado2 === false) {
            echo mysqli_error($c);
        }
        mysqli_commit($c);
    } catch (Exception $ex) {
        mysqli_rollback($c);
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

function registroLocal($username, $passwd, $nombre, $email, $tlf, $idciudad, $direccion, $aforo) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c, MYSQLI_TRANS_START_READ_WRITE);
        $query = "insert into usuario values (null, '$username', '$passwd', '$nombre', '$email', 'foto', '$tlf', $idciudad, 2)";
        $resultado = mysqli_query($c, $query);
        if ($resultado === false) {
            echo mysqli_error($c);
        }
        $id = mysqli_insert_id($c);
        $query2 = "insert into lokal values ($id, $aforo, '$direccion')";
        $resultado2 = mysqli_query($c, $query2);
        if ($resultado2 === false) {
            echo mysqli_error($c);
        }
        mysqli_commit($c);
    } catch (Exception $ex) {
        mysqli_rollback($c);
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

function registroMusico($username, $passwd, $nombre, $email, $tlf, $idciudad, $idgenero, $web, $componentes) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c, MYSQLI_TRANS_START_READ_WRITE);
        $query = "insert into usuario values (null, '$username', '$passwd', '$nombre', '$email', 'foto', '$tlf', $idciudad, 3)";
        $resultado = mysqli_query($c, $query);
        if ($resultado === false) {
            echo mysqli_error($c);
        }
        $id = mysqli_insert_id($c);
        $query2 = "insert into musico values ($id, '$idgenero', '$web', '$componentes')";
        $resultado2 = mysqli_query($c, $query2);
        if ($resultado2 === false) {
            echo mysqli_error($c);
        }
        mysqli_commit($c);
        return 'ok';
    } catch (Exception $ex) {
        mysqli_rollback($c);
        return null;
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

function selectAllGeneros() {
    $select = "SELECT * FROM daw1mg6.genero;";
    return query($select);
}

function existeMail($mail) {
    $select = "select * from usuario where email = '$mail';";
    if (mysqli_num_rows(query($select)) > 0) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
//                             LISTA DE LOCALES 
//------------------------------------------------------------------------------
function listaLocales() {
    $select = "select u.username as 'Username', u.nombre as 'Nombre', u.email as 'E-mail', 
u.telefono as 'Teléfono', c.nombre as 'Ciudad', l.aforo as 'Aforo', l.ubicacion as 'Direccion'
from usuario u inner join lokal l on u.idusuario = l.idlocal
inner join ciudad c on u.idciudad = c.idciudad
where u.tipo = 2
order by u.idciudad;";
    return query($select);
}

////////////////////////////////////////////////////////////////////////////////
//                               LISTAR DATOS
////////////////////////////////////////////////////////////////////////////////

function selectConciertosRealizados($idusuario) {
    $query = "select * from concierto where estadoConcierto = 2 and fecha < now()
and idconcierto not in (select idconcierto from votar_concierto where idfan = $idusuario)";
    return query($query);
}

function selectMusicosAVotar($idusuario) {
    $query = "select idusuario, username from usuario 
        where idusuario in (select idmusico from musico)
        and idusuario not in (select idmusico from votar_musico
        where idfan = $idusuario);";
    
    return query($query);
}

function listarCiudades() {
    $select = "select * from ciudad";
    return query($select);
}

function recoger_ID_Usuario($username) {
    $select = "select idusuario from usuario where username = '$username'";
    $resultado = query($select);
    if (!$resultado) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_row($resultado);
    }
    return $fila[0];
}

function datosUsuario($idUsuario) {
    $select = "select * from usuario where idusuario = $idUsuario";
    $resultado = query($select);
    $fila = mysqli_fetch_assoc($resultado);
    return $fila;
}

function datosFan($idUsuario) {
    $select = "select * from fan where idfan = '$idUsuario'";
    $query = query($select);
    $fila = mysqli_fetch_assoc($query);
    return $fila;
}

function datosMusico($idUsuario) {
    $select = "select * from musico where idmusico = '$idUsuario'";
    $query = query($select);
    $fila = mysqli_fetch_assoc($query);
    return $fila;
}

function datosLocal($idUsuario) {
    $select = "select * from lokal where idlocal = '$idUsuario'";
    $query = query($select);
    $fila = mysqli_fetch_assoc($query);
    return $fila;
}

///////////////////////////////////////////////////
function selectAllMusicos() {
    $c = conectar();
    $select = "select * from usuario where tipo = 3";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function listaDeMusicos() {
    $c = conectar();
    $select = "select usuario.nombre, genero.nombre as 'genero', usuario.email, musico.web , usuario.telefono from usuario 
               inner join musico on usuario.idusuario=musico.idmusico
               inner join genero on genero.idgenero=musico.idgenero
               order by genero.nombre;";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function selectAllConciertos() {
    $c = conectar();
    $select = "select * from concierto where concierto.estadoConcierto = '2';";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function selectConciertosVotados() {
    $c = conectar();
    $select = "select * from votar_concierto;";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function selectMusicosVotados() {
    $c = conectar();
    $select = "select * from votar_musico;";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function listaDeConciertos() {
    $c = conectar();
    $select = "select concierto.nombre, usuario.nombre as 'musico', genero.nombre as 'genero', concierto.fecha, concierto.hora, concierto.entrada from concierto 
               inner join usuario on concierto.idmusico=usuario.idusuario
               inner join genero on genero.idgenero=concierto.idgenero
               where concierto.estadoConcierto = '2';";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function topConciertos() {
    $c = conectar();
    $select = "select *, sum(puntuacion) as 'puntuacion' from votar_concierto
inner join concierto on concierto.idconcierto = votar_concierto.idconcierto
group by votar_concierto.idconcierto
order by sum(puntuacion) desc
limit 5";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function topMusicos() {
    $c = conectar();
    $select = "select sum(vm.puntuacion) as 'puntuacion', m.nombre from votar_musico vm 
inner join usuario m on vm.idmusico = m.idusuario
group by vm.idmusico
order by sum(vm.puntuacion) desc
limit 5;";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function generosDisponibles() {
    $c = conectar();
    $select = "select distinct genero.nombre from genero 
               inner join musico on genero.idgenero=musico.idgenero
               inner join usuario on usuario.idusuario=musico.idmusico
               order by genero.nombre";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function selectPorGeneros($nombreGenero) {
    $c = conectar();
    $select = "select usuario.nombre, genero.nombre as 'genero', usuario.email, musico.web , usuario.telefono from usuario 
               inner join musico on usuario.idusuario=musico.idmusico
               inner join genero on genero.idgenero=musico.idgenero
               where genero.nombre = '$nombreGenero' order by genero.nombre ";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

////////////////////////////////////////////////////////////////////////////////
//                               ACTUALIZAR DATOS
////////////////////////////////////////////////////////////////////////////////

function getTipo($username) {
    $select = "select tipo from usuario where nombre = '$username'";
    $row = mysqli_fetch_assoc(query($select));
    return $row["tipo"];
}

function cambiarPassword($idUsuario, $newPassword) {
    $c = conectar();
    $encrypt = password_hash($newPassword, PASSWORD_DEFAULT);
    $update = "update usuario set passwrd = '$encrypt' where idusuario = $idUsuario";
    if (mysqli_query($c, $update)) {
        $resultado = true;
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function modificarAdmin($email, $telefono, $ciudad, $nombre, $idUsuario) {
    $c = conectar();
    $modificar = "update usuario set email = '$email', telefono = '$telefono', idciudad = '$ciudad', nombre = '$nombre'"
            . " where idusuario = $idUsuario";
    if (mysqli_query($c, $modificar)) {
        $resultado = true;
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function modificarFan($email, $telefono, $ciudad, $nombre, $apellidos, $idUsuario) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c);
        $modificar1 = "update usuario set email = '$email', telefono = '$telefono', idciudad = '$ciudad', nombre = '$nombre' where idusuario = $idUsuario";
        $resultado1 = mysqli_query($c, $modificar1);
        $modificar2 = "update fan set apellidos = '$apellidos' where idfan = $idUsuario";
        $resultado2 = mysqli_query($c, $modificar2);
        if ($resultado1 && $resultado2) {
            echo "Datos Actualizados<br><br>";
        }
        mysqli_commit($c);
    } catch (Exception $ex) {
        mysqli_rollback($c);
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

function modificarLocal($email, $telefono, $ciudad, $nombre, $direccion, $aforo, $idUsuario) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c);
        $modificar1 = "update usuario set email = '$email', telefono = '$telefono', idciudad = '$ciudad', nombre = '$nombre' where idusuario = $idUsuario";
        $resultado1 = mysqli_query($c, $modificar1);
        $modificar2 = "update lokal set ubicacion = '$direccion', aforo = $aforo where idlocal = $idUsuario";
        $resultado2 = mysqli_query($c, $modificar2);
        if ($resultado1 && $resultado2) {
            echo "Datos Actualizados<br>";
        }
        mysqli_commit($c);
    } catch (Exception $ex) {
        mysqli_rollback($c);
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

function modificarMusico($email, $telefono, $ciudad, $nombre, $web, $componentes, $idUsuario) {
    $c = conectar();
    try {
        mysqli_autocommit($c, false);
        mysqli_begin_transaction($c);
        $modificar1 = "update usuario set email = '$email', telefono = '$telefono', idciudad = '$ciudad', nombre = '$nombre' where idusuario = $idUsuario";
        $resultado1 = mysqli_query($c, $modificar1);
        $modificar2 = "update musico set web = '$web', componentes = $componentes where idmusico = $idUsuario";
        $resultado2 = mysqli_query($c, $modificar2);
        if ($resultado1 && $resultado2) {
            echo "Datos Actualizados<br>";
        }
        mysqli_commit($c);
    } catch (Exception $ex) {
        mysqli_rollback($c);
    }
    mysqli_autocommit($c, true);
    mysqli_close($c);
}

function recogerFoto($idUsuario) {
    $select = "select foto from usuario where idusuario = $idUsuario";
    $resultado = query($select);
    if (!$resultado) {
        $resultado = mysqli_error($c);
    } else {
        $fila = mysqli_fetch_row($resultado);
    }
    return $fila[0];
}

function cambiarFoto($idUsuario, $foto) {
    $c = conectar();
    $update = "update usuario set foto = '$foto' where idusuario = $idUsuario";
    if (mysqli_query($c, $update)) {
        $resultado = true;
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function conciertoSlider() {
    $c = conectar();
    $select = "select concierto.nombre, concierto.fecha, usuario.foto 
               from concierto inner join usuario 
               ON usuario.idusuario = concierto.idmusico where concierto.fecha > current_date()
               and concierto.estadoConcierto = 2 order by concierto.fecha asc limit 3";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function likeConcierto($idconcierto, $idusuario) {
    $query = "insert into votar_concierto values ($idusuario, $idconcierto, 1 )";
    query($query);
}

function likeMusico($idmusico, $idfan) {

    $query = "insert into votar_musico values ($idfan, $idmusico, 1 )";
    query($query);
}
