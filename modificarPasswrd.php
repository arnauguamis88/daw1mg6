<?php
require 'bbdd/bbdd.php';
session_start();
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    $tipo = getTipo($username);
    $idUsuario = recoger_ID_Usuario($username);
    ?>
    <html>
        <head>
            <meta charset="UTF-8">
            <link href="https://fonts.googleapis.com/css?family=Quantico&display=swap" rel="stylesheet">
            <link href="estilos/modificar_estilos.css" rel="stylesheet" type="text/css"/>
            <title>Modificar</title>
        </head>
        <body>
            <div id="modificarPass">
                <h1> ACTUALIZAR CONTRASEÑA </h1><br>
                <form method="POST">    
                    <input type="password" placeholder="Antigua contraseña" name="antiguaPass" minlenght="3" maxlength="10" required><br><br><br>
                    <input type="password" placeholder="Nueva contraseña" name="nuevaPass" minlength="3" maxlength="10" required><br><br><br>
                    <input type="password" placeholder="Verificar nueva contraseña" name="nuevaPassV" minlenght="3" maxlength="10" required><br><br><br>

                    <input class="submit" type="submit" name="cambiar" value="Cambiar"><br>
                </form>
                <?php
                if (isset($_POST["cambiar"])) {

                    $antiguaPass = $_POST["antiguaPass"];
                    $nuevaPass = $_POST["nuevaPass"];
                    $nuevaPassV = $_POST["nuevaPassV"];

                    if (verificarDatos($username, $antiguaPass) === true && $nuevaPass === $nuevaPassV) {
                        if (cambiarPassword($idUsuario, $nuevaPass)) {
                            echo "Contraseña cambiada<br><br>";
                        } else {
                            echo $resultado;
                        }
                    } else {
                        echo "La contraseña es incorrecta<br><br>";
                    }
                }
                ?>
        </body>
        <?php
        switch ($tipo) {
            case 0:
                ?>
                <a href = "perfil_Admin.php">VOLVER</a><br>
                <?php
                break;
            case 1:
                ?>
                <a href = "perfil_fan.php">VOLVER</a><br>
                <?php
                break;
            case 2:
                ?>
                <a href = "perfil_local.php">VOLVER</a><br>
                <?php
                break;
            case 3:
                ?>
                <a href = "perfil_musico.php">VOLVER</a><br>
                <?php
                break;
        }
        ?>
    </div>
    </html>
    <?php
} else {
    echo "No tienes permiso para acceder a esta página";
}